<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['middleware' => 'cors'], function(Router $api){
        $api->group(['prefix' => 'user'], function(Router $api) {
            $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
            $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

            $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');

            $api->post('logout', 'App\\Api\\V1\\Controllers\\LogoutController@logout');
            $api->get('info', 'App\\Api\\V1\\Controllers\\UserController@info');
            $api->post('resend', 'App\\Api\\V1\\Controllers\\UserController@resend');
            $api->get('required_additional_fields', 'App\\Api\\V1\\Controllers\\UserController@required_additional_fields');
            $api->post('required_additional_fields', 'App\\Api\\V1\\Controllers\\UserController@post_required_additional_fields');

            $api->get('profile', 'App\\Api\\V1\\Controllers\\UserController@profile');

            $api->group(['prefix' => 'reserves'], function(Router $api) {
                $api->get('', 'App\\Api\\V1\\Controllers\\UserReservesController@index');
                $api->post('{reserve_code}/edit', 'App\\Api\\V1\\Controllers\\UserReservesController@edit');
                $api->post('{reserve_code}/cancel', 'App\\Api\\V1\\Controllers\\UserReservesController@cancel');
            });
            
            $api->get('favourites', 'App\\Api\\V1\\Controllers\\UserFavouritesController@index');

            $api->group(['prefix' => 'settings'], function(Router $api) {
                $api->get('', 'App\\Api\\V1\\Controllers\\UserSettingsController@index');
                $api->post('', 'App\\Api\\V1\\Controllers\\UserSettingsController@save');

                $api->post('email', 'App\\Api\\V1\\Controllers\\UserSettingsController@change_email');
                $api->post('password', 'App\\Api\\V1\\Controllers\\UserSettingsController@change_password');

                $api->post('delete_account', 'App\\Api\\V1\\Controllers\\UserSettingsController@delete_account');
            });
        });

        $api->resource('cities', '\\App\\Api\\V1\\Controllers\\CitiesController', [
            'only' => ['index']
        ]);

        $api->group(['prefix' => 'c/{city_id}'], function(Router $api) {
            $api->group(['prefix' => 'discover', 'as' => 'discover.'], function(Router $api) {
                $api->get('', 'App\\Api\\V1\\Controllers\\DiscoverController@index');
            });

            $api->group(['prefix' => 'search', 'as' => 'search.'], function(Router $api) {
                $api->get('', 'App\\Api\\V1\\Controllers\\SearchController@search');

                $api->get('outstading', 'App\\Api\\V1\\Controllers\\SearchController@outstading');
                $api->get('term/{q}', 'App\\Api\\V1\\Controllers\\SearchController@term');
            });
        });

        $api->group(['prefix' => 'restaurant/{restaurant_id}'], function(Router $api) {
            $api->get('', 'App\\Api\\V1\\Controllers\\RestaurantsController@show');

            $api->post('favourite/{value}', 'App\\Api\\V1\\Controllers\\RestaurantsController@favourite');

            $api->get('reserve/{date}', 'App\\Api\\V1\\Controllers\\RestaurantsController@reserve');
            $api->post('reserve/{date}', 'App\\Api\\V1\\Controllers\\RestaurantsController@post_reserve');

            $api->post('review', 'App\\Api\\V1\\Controllers\\RestaurantsController@review');
        });

        $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
            $api->get('protected', function() {
                return response()->json([
                    'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
                ]);
            });
        });
    });
});
