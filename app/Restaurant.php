<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;
use Carbon\Carbon;
use Sofa\Eloquence\Eloquence;
use Illuminate\Support\Facades\DB;
use App\RestaurantScheduler;

class Restaurant extends Model
{   
    use Eloquence;

    protected $table = 'restaurant';

    protected $searchableColumns = [
        'name' => 20, 
        'address' => 15,
        'description' => 15,
        'type.name' => 5,
        'tags.name' => 5,
        'menu.title' => 5,
        'menu.items.name' => 5,
        'menu.items.description' => 5,
    ];

	/**
     * Protected attributes that CANNOT be mass assigned.
     *
     * @var array
     */
	protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name', 'address', 'phone', 'description', 'city_id', 'restaurant_type_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get the restaurant type record associated with the restaurant.
     */
    public function type()
    {	
    	//echo \App\Restaurant::all()->first()->type->name;
    	return $this->hasOne('App\RestaurantType', 'id','restaurant_type_id');
    }

    /**
     * Get the city record associated with the restaurant.
     */
    public function city()
    {	
    	return $this->hasOne('App\City', 'id','city_id');
    }

    /**
     * Get photo records associated with the restaurant.
     */
    public function photos(){
        return $this->hasMany('App\RestaurantPhoto')->orderBy('main', 'DESC')->orderBy('created_at', 'DESC');
    }

    /**
     * Get tags records associated with the restaurant.
     */
    public function tags()
    {	
    	return $this->belongsToMany('App\RestaurantTag');
    }

    public function changes()
    {
        return $this->hasOne('App\RestaurantChanges', 'restaurant_id', 'id');
    }

    public function i()
    {   
        $changes_info = \App\RestaurantChanges::find($this->id);
        $info = new \stdClass;

        $info->name = ($changes_info && $changes_info->name) ? $changes_info->name : $this->name;
        $info->address = ($changes_info && $changes_info->address) ? $changes_info->address : $this->address;
        $info->phone = ($changes_info && $changes_info->phone) ? $changes_info->phone : $this->phone;
        $info->description = ($changes_info && $changes_info->description) ? $changes_info->description : $this->description;
        $info->city_id = ($changes_info && $changes_info->city_id) ? $changes_info->city_id : $this->city_id;

        return $info;
    }

    public function hasPendingChanges()
    {
        return $this->changes()->exists();
    }

    public function isApprobed()
    {
        return $this->approbed == 1;
    }
    
    /**
     * Get scheduler records associated with the restaurant.
     */
    public function schedulers(){
        return $this->hasMany('App\RestaurantScheduler')->orderBy('date_start', 'DESC');
    }

    public function schedulers_days(){
        return $this->hasManyThrough('App\RestaurantSchedulerDay', 'App\RestaurantScheduler', 'restaurant_id', 'scheduler_id', 'id', 'id');
    }

    /**
     * Get managers records associated with the restaurant.
     */
    public function managers(){
        return $this->belongsToMany('App\Manager', 'restaurant_manager')->withPivot('role', 'activation');
    }

    /**
    * Get average price
    */
    public function average_price()
    {   
        $sections = $this->menu->all();
        $avg = 0;
        foreach ($sections as $s) {
            $avg += $s->items()->avg('price');
        }

        if(count($sections) > 0)
            $avg = round($avg/count($sections),2);

        return $avg;
    }

    /**
     * Get menu records associated with the restaurant.
     */
    public function menu()
    {
        return $this->hasMany('App\RestaurantMenuSection')->orderBy('order', 'ASC');
    }

    public function menu_items()
    {
        return $this->hasManyThrough('App\RestaurantMenuItem', 'App\RestaurantMenuSection')->orderBy('order', 'ASC');
    }

    /**
     * Get restaurant rating
     */
    public function rate()
    {
        return ($rate = $this->reviews()->avg('rate')) ? $rate : 0;
    }

    /**
     * Get review records associated with the restaurant.
     */
    public function reviews(){
        return $this->hasMany('App\RestaurantReview')->orderBy('created_at', 'DESC');
    }

    /**
     * Get count of reviews by stars records associated with the restaurant.
     */
    public function rate_count_by_stars(){
        $stars = [];

        for ($i=1; $i <= 5; $i++) { 
            $stars[$i] = $this->reviews()->where('rate', '=', $i)->count();
        }

        return $stars;
    }

    /**
     * Get closed day records associated with the restaurant.
     */
    public function closed_days($start = null, $end = null, $is_equal = false){
        return $this->hasMany('App\RestaurantClosedDay')->where(function($q) use ($start, $end, $is_equal){ // Filtros adicionales
            if($is_equal) { // Si se quiere buscar una fecha en concreto... ($el parametro end se ignorará)
                $q->whereDate('date', '=', $start->toDateString());
            }else{
                if($start !== null) // Si se quiere buscar por fechas iguales o posteriores 
                $q->whereDate('date', '>=', $start->toDateString());
                if($end !== null) // Si se quiere buscar por fechas iguales o anteriores
                $q->whereDate('date', '<=', $end->toDateString());
            }
        })->orderBy('date', 'DESC'); // Mostrar primero los dias mayores
    }

    /**
     * Get reserve records associated with the restaurant.
     */
    public function reserves($date = null){
        return $this->hasMany('App\UserReserve', 'restaurant_id', 'id')->where(function($q) use ($date){ // Filtros adicionales
            if($date !== null) // Si se quiere ver las reservas de un determinado día
            $q->whereRaw("DATE(date) = '$date'");
        })->orderBy('date', 'DESC');
    }

    public function was_client()
    {
        $result = null;

        if(Auth::check())
            $result = $this->reserves()->whereNotNull('date_end')->where(['user_id' => Auth::id(), 'canceled' => 0])->exists();

            return $result;
        }

    /**
     * Get if restaurant is favourite for the user.
     */
    public function is_favourite()
    {
        $favourite = null;

        if(Auth::check())
            $favourite = DB::table('user_restaurant_favourite')->where(['restaurant_id' => $this->id, 'user_id' => Auth::id()])->exists();

            return $favourite;
        }

    /**
     * Get available days for reserve
     */
    public function available_days($hide_full_days = true)
    {  
        // Solo se permite reservar desde el día actual hasta un máximo de 3 meses
        $start_search_date = Carbon::now()->addDay(); // Fecha de inicio para poder reservar
        $finish_search_date = (Carbon::now())->addMonths(3); // Fecha de fin para poder reservar

        $available_days = null; // Valor por defecto de la respuesta (Si no hay horarios para ningúna fecha proxima)
        $schedulers =  RestaurantScheduler::whereFromDate($this->id, array($start_search_date, $finish_search_date));// Obtener horarios que se encuentren entre las fechas permitidas para reservar

        if($schedulers->count() > 0){ // Si hay horarios continuamos
            $available_days = []; // Array para los días que se permiten reservas

            $today = new Carbon(); // Día actual
            if(count($this->available_hours($today)) > 0) // Si hay horas disponibles para reservar hoy
                $available_days[] = $today->toDateString(); // añadir día actual al array de disponibles

            $full_days = []; // Array para los días completos o cerrados
            foreach ($this->closed_days($start_search_date, $finish_search_date)->pluck('date')->toArray() as $date){ // Recorrer dias cerrados que pertenezacn a las fechas posibles de reserva
                $full_days[] = $date->toDateString(); // Añadir días al array
            }
            
            if($hide_full_days){
                foreach (DB::select(DB::raw('SELECT `date` FROM restaurant_full_day WHERE restaurant_id = :id AND `date` >= DATE(NOW()) AND `date` < DATE(DATE_ADD(NOW(), INTERVAL 3 MONTH))'), ['id' => $this->id]) as $fdate){ // Consultar días completos
                    $full_days[] = $fdate->date; // Añadir al array
                }
            }

            $schedulers = $schedulers->orderBy('date_start', 'ASC')->get(); // Obtener horarios de los posibles días de reserva
            foreach ($schedulers as $sch) { // recorrer horarios

                $date_i_start = ($sch->date_start > $start_search_date) ? $sch->date_start : $start_search_date; // Fecha de inicio (El día actual o el día de inicio del horario)
                $date_i_end = (($sch->date_end && $sch->date_end <= $finish_search_date) ? $sch->date_end : $finish_search_date)->addDay(); // (El día de fin de reservas o el dia de fin del horario)
                
                $dwwh = $sch->not_empty_days(); // Obtener días sin horario

                for($date_i = $date_i_start->copy(); $date_i->lte($date_i_end); $date_i->addDay()) { // Recorrer días del horario
                    $day_in_full_day_array = array_search($date_i->toDateString(), $full_days); // Obtener posición de la fecha en el array de 'completos'

                    if($day_in_full_day_array === false){ // Si no se encuentra la fecha en el array de completos
                        if(array_search($date_i->dayOfWeek, $dwwh) !== false) // y tampoco es un dia sin horario
                            $available_days[] = $date_i->toDateString(); // se añade al array de días disponibles
                    }else array_splice($full_days, $day_in_full_day_array, 1); // Si se encuentra: lo eliminamos ya del array de 'completos' (ya no es necesario)
                }
            }
        }

        return $available_days;
    }

    /**
     * Get available hours for reserve
     */
    public function available_hours(Carbon $date, $getFull = false, $num_pers = null)
    {
        $hours = []; // Valor por defecto de la respuesta (Si no hay horario para este día)

        $today = new Carbon(date('Y-m-d')); // Obtener día actual

        if($date->gte($today)){ // Si la fecha a consultar es mayor al día actual continuamos
            $scheduler = RestaurantScheduler::whereFromDate($this->id, $date)->first(); // Obtener el horario al que pertenece la fecha

            if($scheduler !== null && ($sc_hours = $scheduler->day($date->dayOfWeek)) && $sc_hours->count() > 0){ // Si existe el horario y si hay horas en él para ese día

                $reserved_hours = []; // Array para las horas completas
                foreach( // Consultamos las horas completas para ese día y las metemos en el array
                    DB::select(DB::raw('SELECT `time`,`pending_count`, `is_full` FROM restaurant_hour_reserves_count WHERE restaurant_id = :id AND `date` = :date'), ['id' => $this->id, 'date' => $date->toDateString()]) as $hour){
                    $reserved_hours[substr($hour->time, 0, 5)] = ['num_pers' => $hour->pending_count,'is_full' => $hour->is_full];
                    // Quitar horas intermedias
                };

                foreach ($sc_hours->get() as $sc_hour) { // Recorremos los periodos de hora del día
                    $hour_i_start = new Carbon($date->toDateString().' '.$sc_hour->hour_start); // Inicio del periodo
                    $hour_i_end = new Carbon($date->toDateString().' '.$sc_hour->hour_end); // Fin del periodo

                    for($hour_i = $hour_i_start->copy(); $hour_i->lte($hour_i_end); $hour_i->addMinutes(30)) { // Recorrer periodo cada 30 min
                        $h = $hour_i->format('H:i'); // Obtener hora en string

                        if((!$date->isToday() || $hour_i->copy()->addMinutes(30)->format('H:i') > (new Carbon())->format('H:i')) && (!isset($reserved_hours[$h]) || $reserved_hours[$h]['is_full'] == 0 || $getFull)){ // (Si la fecha a consultar no es hoy o la hora no ha pasado ) y la hora no se encuentra en el array de 'reservadas' o no está completa

                        $no_full = true; // Añadir por defecto la hora al array de disponibles
                        $res_count = 0;
                        if(!isset($reserved_hours[$h])){ // Si la hora no figura en la BD (No hay reservas esa hora) buscar horas anteriores que estén en esa hora
                            $hour_start = $date->toDateString().' '.($hour_i->copy()->subMinutes($scheduler->average_time - 30)->format('H:i:s')); // Hora de inicio a buscar
                            $hour_end = $date->toDateString().' '.$h.':00'; // Hora de fin a buscar

                            $res_count = $this->reserves()->where(['canceled' => 0])->where('date','>=', $hour_start)->where('date', '<', $hour_end)->whereNull('date_end')->sum('num_pers');

                            $no_full = ($res_count < $scheduler->capacity || $getFull); // Si el número de reservas que puedan encontrarse en esa hora es mayor al permitido, cambiar valor para no agregar hora al array
                        }

                        if($no_full){
                            $num_pers = $scheduler->capacity - ((isset($reserved_hours[$h])) ? $reserved_hours[$h]['num_pers'] : $res_count); // Calcular personas para reserva disponibles 
                            $hours[] = [
                                'hour' => $h,
                                'num_pers' => $num_pers
                            ]; // Añadir hora al array de disponibles
                        }
                    }
                }
            }
        }
    }

    return $hours;
}

public function check_valid_reserveDate(Carbon $datetime, $num_pers = null)
{   
    $result = false;
    $date_str = $datetime->toDateString();
    $time_str = $datetime->format('H:i:s');

    $scheduler = RestaurantScheduler::whereFromDate($this->id, $date_str)->first();

    if($scheduler !== null && ($sc_hours = $scheduler->day($datetime->dayOfWeek)) && $this->closed_days($datetime, null, true)->count() == 0){

        if($sc_hours->whereTime('hour_start', '<=', $time_str)->whereTime('hour_end', '>=', $time_str)->count() > 0 && (!$datetime->isToday() || $datetime->addMinutes(30)->format('H:i') > (new Carbon())->format('H:i'))){

            if($num_pers === null) $result = true; else{
                $reserve_count = 0;

                $rsh = DB::select(DB::raw('SELECT pending_count FROM restaurant_hour_reserves_count WHERE restaurant_id = :id AND `date` = :date AND `time` = :time'), ['id' => $this->id, 'date' => $date_str, 'time' => $time_str]);

                if(count($rsh) > 0)
                    $reserve_count = $rsh[0]->pending_count;
                else{
                    $hour_start = $date_str.' '.($datetime->copy()->subMinutes($scheduler->average_time - 30)->format('H:i:s')); // Hora de inicio a buscar
                    $hour_end = $datetime->toDateTimeString(); // Hora de fin a buscar

                    $reserve_count = $this->reserves()->where('date','>=', $hour_start)->where('date', '<', $hour_end)->whereNull('date_end')->sum('num_pers');
                }

                $result = ($reserve_count+$num_pers <= $scheduler->capacity);
            }
        }
    }
    return $result;
}
}
