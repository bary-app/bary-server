<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class RestaurantMenuItem extends Model
{
	protected $table = 'restaurant_menu_item';
	public $timestamps = false;

     use Eloquence;

     protected $searchableColumns = [
          'name' => 20, 
          'description' => 15,
          'section.title' => 5
     ];

	/**
     * Protected attributes that CANNOT be mass assigned.
     *
     * @var array
     */
	protected $guarded = ['id'];

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'name', 'description', 'price', 'restaurant_menu_section_id', 'order'
	];

	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
	protected $hidden = ['order', 'restaurant_menu_section_id'];

     /**
     * Get the section record associated with the restaurant.
     */
     public function section()
     {     
          return $this->hasOne('App\RestaurantMenuSection', 'id','restaurant_menu_section_id');
     }
}
