<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RestaurantApproved extends Notification
{
    use Queueable;

    private $restaurant_id;
    private $restaurant_name;
    private $name;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($restaurant_id, $restaurant_name, $name)
    {   
        $this->restaurant_id = $restaurant_id;
        $this->restaurant_name = $restaurant_name;
        $this->name = $name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->subject('Su restaurante ha sido aprobado!')
        ->greeting('Hola '.$this->name.'!')
        ->line('Su restaurante '.$this->restaurant_name.' ya forma parte de nuestra plataforma, por lo tanto los usuarios ya podrán verlo en la web y reservar.')
        ->line('Puede echar un vistazo a sus nuevas reservas o realizar algúnos cambios de última hora desde nuestro portal de administración.')
        ->action('Administrar', url(route('restaurant.index', [$this->restaurant_id])))
        ->line('Esperamos que disfrute de los beneficios que ofrece esta plataforma.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
