<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantTag extends Model
{
	protected $table = 'restaurant_tag';
	public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'id', 'name',
    ];

    public static function stringTagsToArray($stringTags)
    {
        $tags = preg_split('/,/', $stringTags);
        $tags = array_values(array_filter($tags));
        $tags = array_filter(array_map(function($value){
            return ltrim(rtrim($value));
        }, $tags));

        return $tags;
    }
}
