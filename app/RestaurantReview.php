<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantReview extends Model
{
	protected $table = 'restaurant_review';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'user_id', 'restaurant_id', 'title', 'description', 'rate',
	];

	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
	protected $hidden = ['user_id', 'restaurant_id'];

	/**
     * Get the user record associated with the review.
     */
	public function user()
	{	
		return $this->hasOne('App\User', 'id','user_id');
	}

	/**
     * Get the restaurant record associated with the review.
     */
	public function restaurant()
	{	
		return $this->hasOne('App\Restaurant', 'id','restaurant_id');
	}
}
