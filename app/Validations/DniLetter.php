<?php

namespace App\Validations;

use Illuminate\Contracts\Validation\Rule;

class DniLetter implements Rule
{   

    private static function CALCULATE_DNI_LETTER($dni_number){
        $dni_number = str_replace(array('X', 'Y', 'Z'), array(0, 1, 2), $dni_number);   

        $module = $dni_number%23;

        $letters = 'TRWAGMYFPDXBNJZSQVHLCKE';

        return substr($letters, $module, 1);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {   
        $letter = substr($value, -1, 1);
        $number = substr($value, 0, 8);

        return self::CALCULATE_DNI_LETTER($number) == $letter;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'La letra no coincide con el DNI/NIF';
    }
}