<?php

namespace App\Validations;

use Illuminate\Contracts\Validation\Rule;
use App\RestaurantScheduler;


class SchedulerDateNotIn implements Rule
{   
    private $restaurant_id;
    private $same_schedule_id;

    public function __construct($restaurant_id, $same_schedule_id=null)
    {
        $this->restaurant_id = $restaurant_id;
        $this->same_schedule_id = $same_schedule_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {   
        $schedules_count = RestaurantScheduler::where([['restaurant_id', '=', $this->restaurant_id], ['date_start', '<=', $value], ['date_end', '>=', $value], ['id', '!=', $this->same_schedule_id]])->count();

        return ($schedules_count == 0);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Esta fecha ya pertenece a otro calendario.';
    }
}