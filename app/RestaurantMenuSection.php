<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantMenuSection extends Model
{
	protected $table = 'restaurant_menu_section';
	public $timestamps = false;
	public static $ICONS = [
		'banana-split', 'beer-bottle', 'beer', 'birthday-cake', 'bread', 'carrot', 'cheese', 'cookies', 'cooking-pot', 'croissant', 'fish', 'french-fries', 'hamburger', 'honey', 'hot-dog', 'jamon', 'hot-dog', 'jelly', 'paella', 'pear', 'pizza', 'rice-bowl', 'shellfish', 'shushi', 'spaghetti', 'steak', 'taco', 'tapas', 'tea', 'wine-bottle', 'wine',
	];

	/**
     * Protected attributes that CANNOT be mass assigned.
     *
     * @var array
     */
	protected $guarded = ['id'];

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'icon', 'title', 'restaurant_id', 'order'
	];

	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
	protected $hidden = ['order', 'restaurant_id'];

	/**
     * Get items records associated with the menu section.
     */
	public function items(){
		return $this->hasMany('App\RestaurantMenuItem')->orderBy('order', 'ASC');
	}

	/**
     * Get the restaurant record associated with the restaurant.
     */
	public function restaurant()
	{     
		return $this->hasOne('App\Restaurant', 'id','restaurant_id');
	}
}
