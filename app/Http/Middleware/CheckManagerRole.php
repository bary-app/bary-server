<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class CheckManagerRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$role)
    {   

        if (($role == null && \Auth::user()->hasRoleAt($request->restaurant_id) != null) || ( $role != null && in_array(Auth::user()->hasRoleAt($request->restaurant_id), $role))) {
            return $next($request);
        }

        return redirect(route('home'));
    }
}
