<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Restaurant;


class ApproveRestaurantsChangesController extends Controller
{	
	/**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$restaurants = Restaurant::has('changes')->paginate(10);

		return view('admin.changes', ['restaurants' => $restaurants]);
	}

	public function approve($restaurant_id)
	{
		$restaurant = Restaurant::find($restaurant_id);

		$changes = $restaurant->changes;

		if($changes->name)
			$restaurant->name = $changes->name;

		if($changes->address)
			$restaurant->address = $changes->address;

		if($changes->phone)
			$restaurant->phone = $changes->phone;

		if($changes->description)
			$restaurant->description = $changes->description;

		if($changes->city_id)
			$restaurant->city_id = $changes->city_id;

		if($restaurant->save())
			$changes->delete();


		return redirect()->route('admin.changes');
	}
}