<?php

namespace App\Http\Controllers\Assistant;

use App\Restaurant;
use App\City;
use App\RestaurantTag;
use App\RestaurantType;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Notification;
use App\Notifications\RestaurantRegistered;

class CreateController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register restaurant Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new restaurants as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/manager/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {   
        return view('assistant.create', ['cities' => City::all(), 'restaurant_types' => RestaurantType::all()]);
    }


    public function getRestaurantTags(Request $request){
        $term = strtolower($request->input('term'));

        $tags = RestaurantTag::whereRaw("LOWER(restaurant_tag.name) LIKE '%".$term."%'")->get()->toArray();

        return array_column($tags, 'name');
    }   

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function validator(array $data)
    {   
        $validator = Validator::make($data, [
            'name' => 'required|string|max:255',
            'type_id' => 'required|integer|exists:restaurant_type,id',
            'city_id' => 'required|integer|exists:city,id',
            'phone' => 'required|regex:/^(\+)?[0-9]+(\ [0-9]+)?$/',
            'address' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'tags' => [function ($attribute, $value, $fail) {
                if (!empty(trim($value))){
                    $tags = RestaurantTag::stringTagsToArray($value);

                    if(count($tags) > 10)
                        $fail('Has introducido más de 10 etiquetas!');

                    if(count(array_unique($tags)) != count($tags))
                        $fail('No puedes introducir etiquetas duplicadas.');
                }
            }]
        ]);

        return $validator;
    }

    public function register(Request $request)
    {   
        $data = $request->only(['name','type_id', 'city_id', 'phone', 'address', 'description', 'tags']);

        $validator = $this->validator($data)->validate();

        if($this->create($data))
            return redirect($this->redirectTo);
        else {
            echo "Error 500";
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    private function create(array $data)
    {   
        $restaurant = Restaurant::create([
            'name' => $data['name'],
            'phone' => $data['phone'],
            'address' => $data['address'],
            'description' => $data['description'],
            'restaurant_type_id' => $data['type_id'],
            'city_id' => $data['city_id']
        ]);

        if($restaurant !== null){
            $tags = RestaurantTag::stringTagsToArray($data['tags']);

            foreach ($tags as $tagWord) {
                $tag = RestaurantTag::Where(['name' => $tagWord])->first();

                if($tag === null){
                    $tag = new RestaurantTag();
                    $tag->name = $tagWord;
                    $tag->save();
                }

                \DB::insert("INSERT INTO restaurant_restaurant_tag (restaurant_id, restaurant_tag_id) VALUES ($restaurant->id, $tag->id)");
            }
        };
        
        $user_manager = \Auth::user();

        \DB::insert("INSERT INTO restaurant_manager (restaurant_id, manager_id, role) VALUES ($restaurant->id, $user_manager->id, 'A')");

        $user_manager->sendRestaurantRegisteredWelcome($restaurant->id, $restaurant->name);

        return true;
    }
}