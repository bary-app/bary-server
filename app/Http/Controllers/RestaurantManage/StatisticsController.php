<?php

namespace App\Http\Controllers\RestaurantManage;

use App\Http\Controllers\Controller;
use App\City;
use App\Restaurant;
use App\RestaurantScheduler;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class StatisticsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->middleware(['auth', 'role']);
    }

    public function index($restaurant_id)
    {
        $restaurant = Restaurant::find($restaurant_id);

        $month_reserves = [
            [
                'label' => Carbon::today()->format('Y'),
                'data' =>  $this->month_reserves($restaurant, Carbon::today()),
                'backgroundColor' => ['transparent'],
                'borderColor' => ['#4ec1c1']
            ],
            [
                'label' => Carbon::today()->subYear()->format('Y'),
                'data' =>  $this->month_reserves($restaurant, Carbon::today()->subYear()),
                'backgroundColor' => ['transparent'],
                'borderColor' => ['#ffd36d']
            ]
        ];

        $week_average_time = $this->week_average_time($restaurant);
        $total_average_time = $this->total_average_time($restaurant);
        $reserve_types = $this->reserve_types($restaurant);
        return view('restaurant.statistics', ['restaurant' => $restaurant, 'month_reserves' => json_encode($month_reserves), 'week_average_time' => $week_average_time, 'total_average_time' => $total_average_time, 'reserve_types' => $reserve_types]);
    }

    public function month_reserves($restaurant, $year)
    {   

        $months_reserves_el = $restaurant->reserves()->whereRaw('YEAR(?) = YEAR(`date`)', [$year])->where([
            'canceled' => 0
        ])->select(DB::raw('MONTH(`date`) as monthdate'), DB::raw('SUM(`num_pers`) as rcount'))->groupBy('monthdate')->pluck('rcount', 'monthdate');

        $month_reserves = [];
        for ($i=0; $i < 12; $i++) {
            $month_reserves[$i] = (isset($months_reserves_el[$i+1])) ? (int)$months_reserves_el[$i+1] : 0;
        }

        return $month_reserves;
    }

    public function total_average_time($restaurant)
    {
        $current_scheduler = RestaurantScheduler::whereFromDate($restaurant->id, Carbon::today())->first();

        $average_time = 0;

        if($current_scheduler != null){
            $week_average_time_el = $restaurant->reserves()->whereRaw('DATE(?) <= DATE(`date`)', [$current_scheduler->date_start])->where(function($q) use ($current_scheduler) {
                if($current_scheduler->date_end != null)
                    $q->whereRaw('DATE(?) >= DATE(`date`)', [$current_scheduler->date_end]);
            })->where([
                'canceled' => 0
            ])->whereNotNull('date_end')->select(DB::raw('ROUND(AVG((TIME_TO_SEC(TIMEDIFF(`date`, `date_end`))/60)*-1)) as acount'))->get('acount');

            $average_time = $week_average_time_el[0]->acount;
        }

        return $average_time;
    }

    public function week_average_time($restaurant)
    {
        $current_scheduler = RestaurantScheduler::whereFromDate($restaurant->id, Carbon::today())->first();

        $week_average_time = [];

        if($current_scheduler != null){

            $week_average_time_el = $restaurant->reserves()->whereRaw('DATE(?) <= DATE(`date`)', [$current_scheduler->date_start])->where(function($q) use ($current_scheduler) {
                if($current_scheduler->date_end != null)
                    $q->whereRaw('DATE(?) >= DATE(`date`)', [$current_scheduler->date_end]);
            })->where([
                'canceled' => 0
            ])->whereNotNull('date_end')->select(DB::raw('WEEKDAY(`date`) as weekdate'), DB::raw('ROUND(AVG((TIME_TO_SEC(TIMEDIFF(`date`, `date_end`))/60)*-1)) as rcount'))->groupBy('weekdate')->pluck('rcount', 'weekdate');
        }

        for ($i=0; $i < 7; $i++) {
            $week_average_time[$i] = (isset($week_average_time_el[$i])) ? (int)$week_average_time_el[$i] : 0;  
        }

        return json_encode($week_average_time);
    }

    public function reserve_types($restaurant)
    {
        $current_scheduler = RestaurantScheduler::whereFromDate($restaurant->id, Carbon::today())->first();

        $types_counts = [0,0,0];

        if($current_scheduler != null){
            $reserve_types_count_el = $restaurant->reserves()->whereRaw('DATE(?) <= DATE(`date`)', [$current_scheduler->date_start])->where(function($q) use ($current_scheduler) {
                if($current_scheduler->date_end != null)
                    $q->whereRaw('DATE(?) >= DATE(`date`)', [$current_scheduler->date_end]);
            })->select(DB::raw('COUNT(date_end) as finalized'), DB::raw('SUM(IF(canceled=1, 1, 0)) as canceled'), DB::raw('SUM(IF(date_end IS NULL, 1, 0)) as not_finalized'))->get()[0];

            $types_counts = [(int)$reserve_types_count_el->finalized, (int)$reserve_types_count_el->canceled, (int)$reserve_types_count_el->not_finalized];
        }

        return json_encode($types_counts);
    }
}