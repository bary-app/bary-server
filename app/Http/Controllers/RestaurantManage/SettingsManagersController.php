<?php

namespace App\Http\Controllers\RestaurantManage;

use App\Http\Controllers\Controller;
use Auth;
use App\City;
use App\Restaurant;
use App\Manager;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\Notifications\ManagerPetition;

class SettingsManagersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->middleware(['auth', 'role:A'], ['except' => ['confirmation']]);
    }

    public function index($restaurant_id)
    {
        $restaurant = Restaurant::find($restaurant_id);

        $admins = $restaurant->managers()->where(['role' => 'A'])->count();

        return view('restaurant.settings.managers', ['restaurant' => $restaurant, 'num_admins' => $admins]);
    }

    public function ajax_get_users(Request $request){
        $term = strtolower($request->input('term'));

        $managers = Manager::whereRaw("LOWER(name) LIKE '%".$term."%' OR LOWER(email) LIKE '%".$term."%'")->select(array('id', 'name', 'email'))->get()->toArray();

        return $managers;
    } 

    public function save($restaurant_id, Request $request)
    {
        $data = $request->only(['user']);

        $restaurant = Restaurant::find($restaurant_id);

        $current_managers = $restaurant->managers()->pluck('activation', 'id');
        $passed_managers = [];

        foreach ($data['user'] as $user) {
            $user = json_decode($user);

            $passed_managers[$user->id] = NULL;

            if(isset($current_managers[$user->id])){
                if($current_managers[$user->id] === NULL && Auth::id() != $user->id)
                    DB::update("UPDATE restaurant_manager SET role='$user->role' WHERE manager_id=$user->id AND restaurant_id=$restaurant_id");

            }else{
                    $activation_code = sha1(mt_rand().$user->id);

                    Manager::find($user->id)->notify(new ManagerPetition($restaurant->name, $user->role, $activation_code));

                    DB::insert("INSERT INTO restaurant_manager (manager_id, role, activation, restaurant_id) VALUES ($user->id, '$user->role', '$activation_code', $restaurant_id)");
            };

            $current_managers[$user->id] = NULL;
        }

        $delete_managers = array_diff_assoc($current_managers->toArray(), $passed_managers);

        foreach ($delete_managers as $key => $value) {
            DB::delete("DELETE FROM restaurant_manager WHERE manager_id=$key AND restaurant_id=$restaurant_id");
        }
        
        return redirect()->route('restaurant.settings.managers', [$restaurant_id]);
    }

    public function confirmation($code)
    {
        $result = DB::update("UPDATE restaurant_manager SET activation=NULL WHERE activation='$code'");

        if($result)
            return redirect()->route('home');
        else
            return view('c.message', ['message' => 'Error al intentar confirmar... Tal vez ingresó un código erróneo o su aprobación expiró.', 'type' => 'error', 'redirect' => false]);
    }
}