<?php

namespace App\Http\Controllers\RestaurantManage;

use App\Http\Controllers\Controller;
use App\Restaurant;
use App\RestaurantMenuSection;
use App\RestaurantMenuItem;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->middleware(['auth', 'role'], ['only' => ['index']]);
        
        $this->middleware(['auth', 'role:A'], ['except' => ['index']]);
    }

    /**
     * Show the menu restaurant manage.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($restaurant_id)
    {	
    	$restaurant = Restaurant::find($restaurant_id);

    	return view('restaurant.menu', ['restaurant' => $restaurant]);
    }

    private function json_array_to_objects($arr){
        return array_map(function($val){
            return json_decode($val);
        }, $arr);
    }

    /**
     * Save the menu restaurant.
     *
     * @return \Illuminate\Http\Response
     */
    public function save($restaurant_id, Request $request)
    {      
        $restaurant = Restaurant::find($restaurant_id);

        $data = $request->only(['menu_section', 'menu_item']);
        $data['menu_section'] = (isset($data['menu_section'])) ? $this->json_array_to_objects($data['menu_section']) : [];
        $data['menu_item'] = (isset($data['menu_item'])) ? $this->json_array_to_objects($data['menu_item']) : [];

        $new_menu_sections = [];
        $deleted_menu_sections = [];
        if(isset($data['menu_section'][0]->order))
            $restaurant->menu()->update(['order' => DB::raw('`order` * -1')]);
        foreach ($data['menu_section'] as $index => $menu_section) {
            if(isset($menu_section->deleted))
                $deleted_menu_sections[$menu_section->id] = 1;
            else{
                if($menu_section->id < 0){

                    $rms = new RestaurantMenuSection();

                    $rms->restaurant_id = $restaurant_id;

                    if(isset($menu_section->icon))
                        $rms->icon = $menu_section->icon;

                    $rms->title = $menu_section->title;
                    $rms->order = $menu_section->order;

                    if(!$rms->save())
                        die('Error!');

                    $new_menu_sections[$menu_section->id] = $rms->id;

                }else {
                    $section_data = [];

                    if(isset($menu_section->icon))
                        $section_data['icon'] = $menu_section->icon;

                    if(isset($menu_section->title))
                        $section_data['title'] = $menu_section->title;

                    if(isset($menu_section->order))
                        $section_data['order'] = $menu_section->order;

                    if(count($section_data) > 0)
                        RestaurantMenuSection::find($menu_section->id)->update($section_data);
                }
            }
        }

        $menu_sections_for_order = [];
        foreach ($data['menu_item'] as $ms) {
            if(isset($ms->order)){
                $section_id = 0;
                if(isset($ms->section_id))
                    $section_id = $ms->section_id;
                else
                    $section_id = RestaurantMenuItem::find($ms->id)->restaurant_menu_section_id;

                if($section_id > 0 && !isset($menu_sections_for_order[$section_id])){
                    RestaurantMenuItem::where(['restaurant_menu_section_id' => $section_id])->update(['order' => DB::raw('`order` * -1')]);
                    $menu_sections_for_order[$section_id] = 1;
                }
            }
        }

        foreach ($data['menu_item'] as $menu_item) {
            if(isset($menu_item->section_id) && $menu_item->section_id < 0 && !isset($deleted_menu_sections[$menu_item->section_id]))
                $menu_item->section_id = $new_menu_sections[$menu_item->section_id];

            if((isset($menu_item->section_id) && isset($deleted_menu_sections[$menu_item->section_id]))||isset($menu_item->deleted)){
                if($menu_item->id > 0)
                    RestaurantMenuItem::find($menu_item->id)->delete();
            }else{
                if($menu_item->id < 0){
                    $rmi = new RestaurantMenuItem();

                    $rmi->restaurant_menu_section_id = $menu_item->section_id;

                    $rmi->name = $menu_item->name;
                    $rmi->description = $menu_item->description;
                    $rmi->price = $menu_item->price;
                    $rmi->order = $menu_item->order;

                    $rmi->save();
                }else{
                    $item_data = [];

                    if(isset($menu_item->name))
                        $item_data['name'] = $menu_item->name;

                    if(isset($menu_item->description))
                        $item_data['description'] = $menu_item->description;

                    if(isset($menu_item->price))
                        $item_data['price'] = $menu_item->price;

                    if(isset($menu_item->section_id))
                        $item_data['restaurant_menu_section_id'] = $menu_item->section_id;

                    if(isset($menu_item->order))
                        $item_data['order'] = $menu_item->order;

                    if(count($item_data) > 0)
                        RestaurantMenuItem::find($menu_item->id)->update($item_data);
                }
            }
        }

        // Recorrer secciones restantes (para eliminar)
        foreach ($deleted_menu_sections as $menu_section_id => $value) {
            if($menu_section_id > 0)
                RestaurantMenuSection::find($menu_section_id)->delete();
        }

        return redirect(route('restaurant.menu.index', [$restaurant_id]));
    }
}