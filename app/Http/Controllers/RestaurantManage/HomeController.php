<?php

namespace App\Http\Controllers\RestaurantManage;

use App\Http\Controllers\Controller;
use App\Restaurant;
use App\RestaurantScheduler;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->middleware(['auth', 'role'])->except('apr');

        $this->middleware(['auth', 'role:A'])->only('apr');
    }


    public function week_reserves($restaurant)
    {
        $weeks_reserves_el = $restaurant->reserves()->whereRaw('YEARWEEK(?, 1) = YEARWEEK(`date`, 1)', [Carbon::today()])->where([
            'canceled' => 0
        ])->select(DB::raw('WEEKDAY(`date`) as weekdate'), DB::raw('SUM(`num_pers`) as rcount'))->groupBy('weekdate')->pluck('rcount', 'weekdate');

        $weeks_reserves = [];
        $weeks_reserves[0] = (count($weeks_reserves_el) > 0) ? 0 : 1;
        for ($i=0; $i < 7; $i++) { 
            $weeks_reserves[$i+1] = (isset($weeks_reserves_el[$i])) ? (int)$weeks_reserves_el[$i] : 0;
        }

        return json_encode($weeks_reserves);
    }

    public function ajax_available_days($id){
        $restaurant = Restaurant::find($id);

        $days = [];
        $av_days = $restaurant->available_days(false);
        if($av_days)
            $days = $av_days;
        
        return response()->json($days);
    }

    public function ajax_reserves_in_day($id, $date)
    {
        $restaurant = Restaurant::find($id);

        $date = Carbon::createFromFormat('Y-m-d', $date);
        $sch = RestaurantScheduler::whereFromDate($id, $date)->first();

        return response()->json([
            'hours' => $restaurant->available_hours($date, true),
            'capacity' => $sch->capacity
        ]);
    }

    public function hours_reserves($restaurant)
    {   
        $t = Carbon::today();

        $hours_reserves_el = $restaurant->reserves()->where([
            'canceled' => 0
        ])
        ->whereRaw('YEAR(?) = YEAR(`date`) AND MONTH(?) = MONTH(`date`) AND DAY(?) = DAY(`date`)', [$t,$t,$t])
        ->select(DB::raw("DATE_FORMAT(`date`, '%H:%i') as hourdate"), DB::raw("SUM(`num_pers`) as rcount"))
        ->groupBy('hourdate')
        ->pluck('rcount', 'hourdate');

        $hours_reserves = [];

        $hour = new Carbon();
        $hour->hour = 0;
        $hour->minute = 0;
        for ($i=0; $i < 48; $i++) {
            $hour_str = $hour->format('H:i');

            $hours_reserves[] = (isset($hours_reserves_el[$hour_str])) ? (int)$hours_reserves_el[$hour_str] : 0;
            
            $hour->addMinutes(30);
        }

        return json_encode($hours_reserves);
    }

    /**
     * Show the restaurant manage dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {	
    	$restaurant = Restaurant::find($id);

        $today_reserves = $this->hours_reserves($restaurant);
        return view('restaurant.index', ['restaurant' => $restaurant, 'week_reserves' => $this->week_reserves($restaurant), 'today_reserves' => $today_reserves]);
    }

    public function apr($id)
    {
        $restaurant = Restaurant::find($id);

        if(!$restaurant->isApprobed()){
            $restaurant->approbed = 2;
            $restaurant->save();
        }

        return redirect()->route('restaurant.index', [$id]);
    }
}