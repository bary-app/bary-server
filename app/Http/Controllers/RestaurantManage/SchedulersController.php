<?php

namespace App\Http\Controllers\RestaurantManage;

use App\Http\Controllers\Controller;
use App\Restaurant;
use App\RestaurantScheduler;
use App\RestaurantSchedulerDay;

use App\RestaurantClosedDay;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Validations\SchedulerDateNotIn;

class SchedulersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->middleware(['auth', 'role'], ['only' => ['index', 'organize']]);
        $this->middleware(['auth', 'role:A'], ['except' => ['index', 'organize']]);
    }

    public function show($restaurant_id, $sch_id){
        return view('errors.404');
    }

    /**
     * Show the restaurant schedules.
     *
     * @return HTML
     */
    public function index($restaurant_id)
    {	
    	$restaurant = Restaurant::with('schedulers')->find($restaurant_id);

        return view('restaurant.schedulers.index', ['restaurant' => $restaurant]);
    }

    public function ajax_create($restaurant_id, $sch_id, Request $request){
    	$data = $request->only(['sch_day_hour_start','sch_day_hour_end', 'sch_day_week_day', 'sch_day_code']);
    	$data['sch_id'] = $sch_id;

    	$validator = Validator::make($data, [
    		'sch_id' => 'required|integer|exists:restaurant_scheduler,id',
    		'sch_day_hour_start' => 'required|string',
    		'sch_day_hour_end' => 'required|string',
    		'sch_day_week_day' => 'required|in:L,M,X,J,V,S,D',
    		'sch_day_code' => 'required|string|unique:restaurant_scheduler_day,code'
    	])->validate();

        $result = RestaurantSchedulerDay::create([
          'code' => $data['sch_day_code'],
          'scheduler_id' => $data['sch_id'],
          'hour_start' => $data['sch_day_hour_start'],
          'hour_end' => $data['sch_day_hour_end'],
          'week_day' => array_search($data['sch_day_week_day'], RestaurantSchedulerDay::$WEEK_NAMES)
      ]);

        return ['status' => $result];
    }

    public function ajax_update($restaurant_id, $sch_id, Request $request){
    	$data = $request->only(['sch_day_code', 'sch_day_hour_start','sch_day_hour_end', 'sch_day_week_day']);

    	$validator = Validator::make($data, [
    		'sch_day_code' => 'required|string|exists:restaurant_scheduler_day,code',
    		'sch_day_hour_start' => 'required|string',
    		'sch_day_hour_end' => 'required|string',
    		'sch_day_week_day' => 'required|in:L,M,X,J,V,S,D'
    	])->validate();

    	$result = false;

    	$day = RestaurantSchedulerDay::where(['code' => $data['sch_day_code']]);
    	if($day !== null)
    		$result = $day->update(['week_day' => array_search($data['sch_day_week_day'], RestaurantSchedulerDay::$WEEK_NAMES), 'hour_start' => $data['sch_day_hour_start'], 'hour_end' => $data['sch_day_hour_end']]);

    	return ['status' => $result];
    }

    public function ajax_remove($restaurant_id, $sch_id, Request $request){
    	$data = $request->only(['sch_day_code']);

    	$validator = Validator::make($data, [
    		'sch_day_code' => 'required|string|exists:restaurant_scheduler_day,code'
    	])->validate();

    	$result = RestaurantSchedulerDay::where(['code' => $data['sch_day_code']])->delete();

    	return ['status' => $result];
    }

    /**
     * Edit a restaurant schedule.
     *
     * @return HTML
     */
    public function organize($restaurant_id, $sch_id)
    {	
    	$restaurant = Restaurant::find($restaurant_id);
    	$scheduler = RestaurantScheduler::with('days')->find($sch_id);

    	$scheduler_days = [];

        $now = Carbon::today('Europe/Brussels')->toDateString();

        foreach ($scheduler->days as $day) {

            $hour_start_split = preg_split('/:/', $day->hour_start);
            $hour_end_split = preg_split('/:/', $day->hour_end);

            $scheduler_days[] = [
             'id' => $day->code,
             'resourceId' => RestaurantSchedulerDay::$WEEK_NAMES[$day->week_day],
             'start' => $now.'T'.$hour_start_split[0].':'.$hour_start_split[1],
             'end' => $now.'T'.$hour_end_split[0].':'.$hour_end_split[1],
         ];
     }

     return view('restaurant.schedulers.organize', ['restaurant' => $restaurant, 'scheduler' => $scheduler, 'scheduler_days' => $scheduler_days]);
 }

    /**
    *  Validator for Create or edit a scheduler
    *  @return Validator
    */
    private function validator($restaurant_id, $data, $scheduler_id=null){
        return Validator::make($data, [
            'date_start' => ['required', 'date', new SchedulerDateNotIn($restaurant_id, $scheduler_id)], // No se encuentre en otro calendario del mismo restaurante
            'date_end' => ['date', 'after:'.date($data['date_start']), new SchedulerDateNotIn($restaurant_id, $scheduler_id)], // Que sea mayor a la fecha de inicio y que no se encuentre en otro calendario del mismo restaurante
            'description' => 'required|string',
            'capacity' => 'required|integer|min:25|max:200',
            'average_time' => ['required', 'integer', 'min:30', 'max:180', function($attribute, $value, $fail){
                if($value%5 !== 0)
                    fail('Valor fuera del rango');
            }]
        ]);
    }

    /**
    * Create a restaurant schedule.
    * 
    * @return HTML
    */
    public function create($restaurant_id)
    {
    	$restaurant = Restaurant::find($restaurant_id);

    	return view('restaurant.schedulers.create', ['restaurant' => $restaurant]);
    }

    /**
    * Edit a restaurant schedule.
    * 
    * @return HTML
    */
    public function edit($restaurant_id, $sch_id)
    {
        $restaurant = Restaurant::find($restaurant_id);
        $scheduler = RestaurantScheduler::find($sch_id);

        return view('restaurant.schedulers.create', ['restaurant' => $restaurant, 'scheduler' => $scheduler]);
    }

    private function pre_save($restaurant_id, $data, $scheduler_id=null)
    {   
        /*
        * pass dates from Spanish format to basic
        */
        if(isset($data['date_start']))
            $data['date_start'] = (Carbon::createFromFormat('d/m/Y', $data['date_start']))->toDateString();
        

        if(isset($data['date_end']) && !empty(trim($data['date_end'])))
            $data['date_end'] = (Carbon::createFromFormat('d/m/Y', $data['date_end']))->toDateString();

        /*
        * validate fields
        */
        $this->validator($restaurant_id, $data, $scheduler_id)->validate();

        $unclosed_restaurant_scheduler = RestaurantScheduler::where([['restaurant_id', '=', $restaurant_id], ['date_start', '<=', $data['date_start']], ['id', '!=', $scheduler_id]])->whereNull('date_end')->first(); // Obtener el calendario que pertenezca a las fechas seleccionadas cuya fecha de fin no esté establecida

        if($unclosed_restaurant_scheduler != null){ // si hay alguno
            $date_end_unclosed_rs = (new Carbon($data['date_start']))->addDay()->toDateString(); // restar 1 dia a la fecha de inicio

            if($date_end_unclosed_rs > $unclosed_restaurant_scheduler->date_start){ // si la fecha restada no es menor o igual a la fecha de inicio de este calendario
                $unclosed_restaurant_scheduler->update([
                    'date_end' => $date_end_unclosed_rs // Poner fecha de fin
                ]);
            }else return 'Error';
        }

        if(empty(trim($data['date_end']))){
            $after_restaurant_scheduler = RestaurantScheduler::where([['restaurant_id', '=', $restaurant_id], ['date_start', '>', $data['date_start']], ['id', '!=', $scheduler_id]])->first();
            
            if($after_restaurant_scheduler != null){
                $date_end_after_rs = (new Carbon($after_restaurant_scheduler->date_start))->subDay()->toDateString(); // restar 1 dia a la fecha de inicio

                if($date_end_after_rs > $data['date_start'])
                    $data['date_end'] = $date_end_after_rs;
                else return 'Error';
            }
        }

        return $data;
    }

    /**
    * Create (POST) a restaurant schedule.
    * 
    * @return HTML
    */
    public function store($restaurant_id, Request $request)
    {
        $data = $this->pre_save($restaurant_id, $request->only(['date_start', 'date_end','description', 'capacity', 'average_time']));

    	/*
    	* Crear calendario
    	*/
    	$new_element_properties = [
    		'restaurant_id' => $restaurant_id,
    		'date_start' => $data['date_start'],
    		'description' => $data['description'],
            'capacity' => $data['capacity'],
            'average_time' => $data['average_time']
        ];

        if(!empty(trim($data['date_end'])))
          $new_element_properties['date_end'] = $data['date_end'];

      $new_save = RestaurantScheduler::create($new_element_properties);

      if($new_save)
    		return redirect(route('restaurant.schedulers.organize', [$restaurant_id, $new_save->id])); // Redirigir a la página de organización
    }

    /**
    * Update (PUT) a restaurant schedule.
    * 
    * @return HTML
    */
    public function update($restaurant_id, $sch_id, Request $request)
    {
        $data = $this->pre_save($restaurant_id, $request->only(['date_start', 'date_end','description', 'capacity', 'average_time']), $sch_id);

        /*
        * Actualizar calendario
        */
        $properties = [
            'date_start' => $data['date_start'],
            'description' => $data['description'],
            'capacity' => $data['capacity'],
            'average_time' => $data['average_time']
        ];

        if(!empty(trim($data['date_end'])))
            $properties['date_end'] = $data['date_end'];

        $update = RestaurantScheduler::find($sch_id)->update($properties);
        if($update)
            return redirect(route('restaurant.schedulers.index', $restaurant_id));
    }

    public function destroy($restaurant_id, $sch_id)
    {
        RestaurantScheduler::find($sch_id)->delete();

        return redirect(route('restaurant.schedulers.index', $restaurant_id));
    }


    public function closed_day_store($restaurant_id, Request $request)
    {
        $data = $request->only('date');

         /*
        * pass dates from Spanish format to basic
        */
         if(isset($data['date']))
            $data['date'] = (Carbon::createFromFormat('d/m/Y', $data['date']))->toDateString();

        Validator::make($data, [
            'date' => ['required', 'date', function($attribute, $value, $fail) use ($restaurant_id){
                if(RestaurantClosedDay::where(['date' => $value, 'restaurant_id' => $restaurant_id])->count() > 0)
                    $fail('La fecha ya había sido introducida.');
                }]
            ])->validate();

        if(RestaurantClosedDay::create([
            'restaurant_id' => $restaurant_id,
            'date' => $data['date']
        ])){
            return redirect(route('restaurant.schedulers.index', $restaurant_id));
        }
    }

    public function closed_day_destroy($restaurant_id, $cld_date)
    {
        if(RestaurantClosedDay::where(['date' => $cld_date, 'restaurant_id' => $restaurant_id])->delete()){
            return redirect(route('restaurant.schedulers.index', $restaurant_id));
        }
    }
}
