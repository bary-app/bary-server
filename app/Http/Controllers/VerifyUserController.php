<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use App\User;
use Auth;
use Illuminate\Routing\Controller;

class VerifyUserController extends Controller
{
    /**
     * Log the user in
     *
     * @param LoginRequest $request
     * @param JWTAuth $JWTAuth
     * @return \Illuminate\Http\JsonResponse
     */
    public function verify($code)
    {
        $user = User::where('verification_code',$code)->first();
        $msg = 'Error intentando confirmar el email. Por favor intentelo mas tarde.'; $type = 'error';
        $script = null;

        if(!is_null($user)){
            if($user->is_verified != 1){
                $user->is_verified = 1;
                $user->verification_code = null;
                $user->save();
                
                $msg = 'Email confirmado correctamente!';

                $script = "if(sessionStorage.getItem('user.data')){".
                "var data = JSON.parse(sessionStorage.getItem('user.data'));".
                "data['_confirmed_email'] = 1;".
                "sessionStorage.setItem('user.data', JSON.stringify(data));".
                "}";

                $type = 'success';
            }else{
                $msg = 'El email ya estaba confirmado!';
                $type = 'info';
            }
        }

        return view('c.message', ['message' => $msg, 'type' => $type, 'redirect' => true, 'script' => $script]);
    }
}
