<?php

namespace App\Http\Controllers;

use config;
use App\User;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ResetPwdUserController extends Controller
{
    /**
     * Show reset form
     *
     * @param Token $token
     */
    public function showResetForm($token)
    {   
        $reset = DB::table(config('auth.passwords.users.table'))->where(['token' => $token])->first();

        if($reset == null)
            return view('c.message', ['message' => 'El código de reseteo no es válido', 'type' => 'error', 'redirect' => false]);

        return view('c.reset', ['token' => $token]);
    }

    /**
     * Get a validator for an incoming reset password request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function validator($data)
    {   
        $token = (isset($data['token'])) ? $data['token'] : null;

        return Validator::make($data, [
            'token' => 'required',
            'email' => ['required', 'email', function ($attr, $value, $fail) use ($token)
            {
                if(!DB::table(config('auth.passwords.users.table'))->where(['token' => $token, 'email' => $value])->exists()){
                    return $fail('Código de reseteo no válido para este email');
                }
            }],
            'password' => 'required|confirmed|min:6'
        ]);
    }

    /**
     * Show page error
     */
    private function getError(){
        return view('c.message', ['message' => 'El código de reseteo no es válido o ha caducado. Por favor intentelo más tarde', 'type' => 'error', 'redirect' => true]);
    }

    /**
     * Reset password
     *
     * @param Token $token
     * @param Request $request
     */
    public function reset(Request $request)
    {
        $data = $request->only(['token', 'email', 'password', 'password_confirmation']);

        $this->validator($data)->validate();

        $user = User::where('email', $data['email'])->first();
        if($user === null)
            return $this->getError();

        DB::table(config('auth.passwords.users.table'))->where(['token' => $data['token'], 'email' => $data['email']])->delete();
        $user->password = $data['password'];
        
        if(!$user->save())
            return $this->getError();

        return view('c.message', ['message' => 'Contraseña cambiada con exito.', 'type' => 'success', 'redirect' => true]);
    }
}