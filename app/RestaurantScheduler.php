<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class RestaurantScheduler extends Model
{
	protected $table = 'restaurant_scheduler';
	public $timestamps = false;

	/**
     * Protected attributes that CANNOT be mass assigned.
     *
     * @var array
     */
	protected $guarded = ['id'];

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'restaurant_id', 'date_start', 'date_end', 'description', 'capacity', 'average_time',
	];

	protected $dates = ['date_start', 'date_end'];

	/**
	*  Obtener el horario al que pertenece la fecha u obtener los horarios que pertenecen a un rango de fechas
	*
    * @var eloquent object
	*/
	public static function whereFromDate($restaurant_id, $date){
		$schedulers = self::where(['restaurant_id' => $restaurant_id]);
		
		if(is_array($date)){
			$schedulers->where(function ($q) use ($date){
				$q->where(function($q) use ($date){
					$q->whereDate('date_start', '<=', $date[0]->toDateString())->where(function($q) use ($date){
						$q->whereDate('date_end', '>=', $date[0])->orWhereNull('date_end');
					});
				})->orWhere(function($q) use ($date){
					$q->whereDate('date_start', '<=', $date[1]->toDateString())->where(function($q) use ($date){
						$q->whereDate('date_end', '>=', $date[1])->orWhereNull('date_end');
					});
				});
			});
		}else{
			$schedulers->whereDate('date_start', '<=', $date)->where(function ($q) use ($date){
				$q->whereDate('date_end', '>=', $date)->orWhereNull('date_end');
			});
		}

		return $schedulers;
	}

	public function days(){
		return $this->hasMany('App\RestaurantSchedulerDay', 'scheduler_id', 'id');
	}

	public function not_empty_days()
	{
		return $this->days()->groupBy('week_day')->pluck('week_day')->toArray();
	}

	public function day($day_pos)
	{
		return $this->days()->where('week_day', $day_pos);
	}
}
