<?php

namespace App\Api\V1\Resources;

use Illuminate\Http\Resources\Json\Resource;

class MenuSectionResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {   
        $items = [];
        foreach ($this->items as $item) {
            $items[] = [
                'name' => $item->name,
                'description' => $item->description,
                'price' => $item->price
            ];
        }

        return [
            'icon' => $this->icon,
            'title' => $this->title,
            'items' => $items
        ];
    }
}