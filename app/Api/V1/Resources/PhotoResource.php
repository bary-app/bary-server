<?php

namespace App\Api\V1\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PhotoResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
    	return [
    		'path' => $this->getOriginalUrl(),
    		'thumb' => $this->getThumbUrl(),
    		'description' => $this->description
    	];
    }
}