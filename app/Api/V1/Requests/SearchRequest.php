<?php

namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;

class SearchRequest extends FormRequest
{
	public function rules()
	{
		return [
			'q' => 'string|max:255',
			'date' => 'date_format:"Y-m-d H:i:s"',
			'num_pers' => 'numeric',
			'p' => 'numeric'
		];
	}

	public function authorize()
	{
		return true;
	}
}