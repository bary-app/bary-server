<?php

namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;
use Auth;
use App\User;
use App\Validations\UserPassword;

class UserChangeEmailRequest extends FormRequest
{	

	public function rules()
	{	
		$arr = SignUpRequest::$GLOBAL_RULES;
		$arr['email'][] = 'confirmed';
		$arr['email'][] = function($attr, $value, $fail){

			if(Auth::guard()->user()->email == $value){
				$fail('Este ya es tu correo electrónico.');
			}else{
				$email_exists = User::where('email', '=', $value)->exists();

				if($email_exists)
					$fail('El correo electrónico introducido pertenece a otro usuario.');
			}		
		};

		return array_merge($arr, [
			'password' => ['required', new UserPassword]
		]);
	}

	public function authorize()
	{
		return true;
	}
}