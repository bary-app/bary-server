<?php

namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;
use App\Validations\UserPassword;

class DeleteAccountRequest extends FormRequest
{	

	public function rules()
	{	
		return [
			'password' => ['required', new UserPassword]
		];
	}

	public function authorize()
	{
		return true;
	}
}