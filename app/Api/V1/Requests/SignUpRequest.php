<?php

namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;

class SignUpRequest extends FormRequest
{	
	public static $GLOBAL_RULES = [
		'email' => ['required' ,'email', 'max:255'],
	];

	public function rules()
	{	
		$arr = array_merge(UserDataRequest::$GLOBAL_RULES, self::$GLOBAL_RULES);
		$arr['email'][] = 'unique:user';

		return array_merge($arr, [
			'password' => 'required|confirmed|min:6'
		]);
	}

	public function authorize()
	{
		return true;
	}
}
