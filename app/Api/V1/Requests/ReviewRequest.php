<?php

namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;

class ReviewRequest extends FormRequest
{
	public function rules()
	{
		return [
			'rating' => ['required','numeric','min:1','max:5'],
			'title' => 'string|max:45|required_with:description',
			'description' => 'string|max:250|required_with:title'
		];
	}

	public function authorize()
	{
		return true;
	}
}