<?php

namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;

class EditReserveRequest extends FormRequest
{
	public function rules()
	{
		return [
			'num_pers_additional' => 'required|numeric|min:-5|max:5',
		];
	}

	public function authorize()
	{
		return true;
	}
}