<?php

namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;
use App\Validations\UserPassword;

class UserChangePasswordRequest extends FormRequest
{	

	public function rules()
	{	
		return [
			'password' => ['required', 'confirmed', 'min:6'],
			'old_password' => ['required', new UserPassword]
		];
	}

	public function authorize()
	{
		return true;
	}
}