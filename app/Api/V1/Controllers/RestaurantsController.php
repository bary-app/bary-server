<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;

use Auth; use Carbon\Carbon; use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Api\V1\Resources\RestaurantResource;
use App\Api\V1\Requests\ReserveRequest;
use App\Api\V1\Requests\ReviewRequest;
use App\Restaurant;
use App\RestaurantReview;
use App\UserReserve;
use App\Api\V1\Resources\ReviewResource;

use App\Notifications\ReserveRegistered;

class RestaurantsController extends Controller
{	
	private $restaurant;

	/**
     * Create a new instance.
     *
     * @return void
     */
	public function __construct(Request $request)
	{
		$this->middleware('auth:api', ['only' => ['post_reserve', 'favourite', 'review']]);

		if($request->bearerToken()) // Optional auth
		$this->middleware('auth:api', ['only' => ['show']]);

		// Cargar restaurante
		$this->restaurant = Restaurant::where(['id' => $request->restaurant_id, 'approbed' => 1])->first();
		if($this->restaurant === null)
			abort('404');
	}

	public function show()
	{	
		return response()->json(new RestaurantResource($this->restaurant), 200);
	}

	public function favourite($restaurant_id, $value)
	{	
		$user_id = Auth::id();
		$operation = false;
		$table = DB::table('user_restaurant_favourite');

		if($value == 'add'){
			if($this->restaurant->is_favourite())
				$operation = true;
			else
				$operation = $table->insert([
					'restaurant_id' => $restaurant_id,
					'user_id' => $user_id
				]);
		}else if($value == 'remove'){
			$operation = $table->where(['restaurant_id' => $restaurant_id, 'user_id' => $user_id])->delete();
		}

		if($operation)
			return response()->json(['status' => 1],200);
		else
			return response()->json(['status' => 0], 500);
	}

	public function reserve($restaurant_id, $date)
	{
		$date = new Carbon($date);

		return response()->json(['hours' => $this->restaurant->available_hours($date)], 200);
	}

	public function post_reserve($restaurant_id, $date, ReserveRequest $request)
	{	
		$data = $request->only(['hour', 'num_pers', 'suggestions']);
		$user = Auth::guard()->user();

		$usr = new UserReserve();
		$usr->user_id = $user->id;
		$usr->restaurant_id = $restaurant_id;
		$usr->date = new Carbon($date.' '.$data['hour']);
		$usr->num_pers = $data['num_pers'];
		$usr->suggestions = $data['suggestions'];
		$usr->generateCode(); 

		if($usr->save()){ 
			try{
				$user->notify(new ReserveRegistered(Restaurant::find($restaurant_id)->name, $usr->num_pers, $usr->date));
			}catch(\Exception $exception){}
			
			return response()->json(['status' => 1],200); 

		} else return response()->json(['status' => 0],500);
	}

	public function review($restaurant_id, ReviewRequest $request)
	{
		$data = $request->only(['rating', 'title', 'description']);
		$user = Auth::guard()->user();

		$allowed = UserReserve::where('restaurant_id', '=', $restaurant_id)->whereNotNull('date_end')->where('user_id', '=', Auth::id())->exists();

		if($allowed){
			$result = false;

			$review = null;
			$review_el = RestaurantReview::where(['restaurant_id' => $restaurant_id, 'user_id' => $user->id]);
			if($review_el->exists()){
				$data = [
					'user_id' => $user->id,
					'restaurant_id' => $restaurant_id,
					'rate' => $request['rating'],
					'title' => ((isset($request['title'])) ? $request['title'] : null),
					'description' => ((isset($request['description'])) ? $request['description'] : null)
				];

				$result = $review_el->update($data);
				$review = $review_el->first();
			}else{
				$review = new RestaurantReview();
				$review->restaurant_id = $restaurant_id;
				$review->user_id = $user->id;
				$review->rate = $request['rating'];
				$review->title = (isset($request['title'])) ? $request['title'] : null;
				$review->description = (isset($request['description'])) ? $request['description'] : null;
				$result = $review->save();
			}

			if($result)
				return response()->json(['status' => 1, 'review' => new ReviewResource($review)],200);
		}else return response()->json(['status' => 0],500);
	}
}