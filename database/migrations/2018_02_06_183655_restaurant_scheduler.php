<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RestaurantScheduler extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_scheduler', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('restaurant_id')->unsigned();
            $table->date('date_start');
            $table->date('date_end')->nullable($value = true);
            $table->string('description');
            $table->integer('capacity');
            $table->integer('average_time');
            $table->unique(['restaurant_id', 'date_start'], 'restaurant_sch_unique');

            $table->foreign('restaurant_id')
            ->references('id')
            ->on('restaurant')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('restaurant_scheduler_day', function (Blueprint $table) {
            $table->string('code')->primary();
            $table->integer('scheduler_id')->unsigned();

            $table->integer('week_day')->length(1);
            $table->time('hour_start');
            $table->time('hour_end');

            $table->unique(['scheduler_id', 'week_day', 'hour_start'], 'restaurant_schday_unique');

            $table->foreign('scheduler_id')
            ->references('id')
            ->on('restaurant_scheduler')->onUpdate('cascade')->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('restaurant_scheduler');
       Schema::dropIfExists('restaurant_scheduler_day');
   }
}
