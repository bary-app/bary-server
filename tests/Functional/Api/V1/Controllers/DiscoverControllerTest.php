<?php

namespace App\Functional\Api\V1\Controllers;

use Hash;
use App\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DiscoverControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
    }

    public function testIndex()
    {
        $response = $this->get('api/c/1/discover', [])->assertJsonCount(6)->assertStatus(200)->isOk();
    }
}
