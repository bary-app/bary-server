<?php

namespace App\Functional\Api\V1\Controllers;

use Hash;
use App\User;
use App\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        $user = new User([
            'name' => 'Test',
            'email' => 'test@email.com',
            'password' => '123456'
        ]);

        $user->save();

        DB::table('user_restaurant_favourite')->insert([
            'restaurant_id' => 21,
            'user_id' => $user->id
        ]);

        DB::table('user_restaurant_favourite')->insert([
            'restaurant_id' => 17,
            'user_id' => $user->id
        ]);
    }

    public function testMe()
    {
        $response = $this->post('api/user/login', [
            'email' => 'test@email.com',
            'password' => '123456'
        ]);

        $response->assertStatus(200);

        $responseJSON = json_decode($response->getContent(), true);
        $token = $responseJSON['token'];

        $this->get('api/user/info', [], [
            'Authorization' => 'Bearer ' . $token
        ])->assertJson([
            'name' => 'Test',
            'surname' => '',
            'confirmed_email' => 0
        ])->isOk();
    }

    public function testProfile()
    {
        $response = $this->post('api/user/login', [
            'email' => 'test@email.com',
            'password' => '123456'
        ]);

        $response->assertStatus(200);

        $responseJSON = json_decode($response->getContent(), true);
        $token = $responseJSON['token'];

        $this->get('api/user/profile', [], [
            'Authorization' => 'Bearer ' . $token
        ])->assertJsonStructure([
            'name',
            'surname',
            'email',
            'favourites',
            'reserves'
        ])->assertJsonCount(2, 'favourites')->isOk();
    }
}
