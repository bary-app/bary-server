<?php

namespace App\Functional\Api\V1\Controllers;

use App\User;
use App\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ForgotPasswordControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        $user = new User([
            'name' => 'Test',
            'email' => 'test@email.com',
            'password' => '123456'
        ]);

        $user->save();
    }

    public function testForgotPasswordRecoverySuccessfully()
    {
        $this->post('api/user/recovery', [
            'email' => 'test@email.com'
        ])->assertJson([
            'status' => 1
        ])->isOk();
    }

    public function testForgotPasswordRecoveryReturnsUserNotFoundError()
    {
        $this->post('api/user/recovery', [
            'email' => 'unknown@email.com'
        ])->assertJson([
            'status' => -1
        ])->assertStatus(422);
    }

    public function testForgotPasswordRecoveryReturnsValidationErrors()
    {
        $this->post('api/user/recovery', [
            'email' => 'i am not an email'
        ])->assertJsonStructure([
            'error'
        ])->assertStatus(422);
    }
}
