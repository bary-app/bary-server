<tr>
	<td class="header">
		<table align="center" width="570" cellpadding="0" cellspacing="0">
			<!-- Body content -->
			<tr>
				<td class="header-content">
					<a href="{{ $url }}" class="logo">
						<img src="{{ $url }}/assets/img/sm_icon.png"/> 
						<span class="name">{{ $slot }}</span>
					</a>
				</td>
			</tr>
		</table>
	</td>
</tr>
