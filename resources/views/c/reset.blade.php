@extends('layouts.mini')

@section('content')
<div class="card">
	<div class="card-content">
		<h2 style="margin-bottom: 20px">Resetear contraseña</h2>
		<form class="form" method="POST" action="{{route('c.user.reset.post')}}" style="max-width:400px">
			{{ csrf_field() }}

			@if ($errors->has('email'))
			<div class="msg error">
				{{ $errors->first('email') }}
			</div>
			@endif

			@if ($errors->has('password'))
			<div class="msg error">
				{{ $errors->first('password') }}
			</div>
			@endif

			<input type="hidden" name="token" value="{{$token}}"/>

			<div class="form-field">
				<label>
					<input type="email" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus />
				</label>
			</div>

			<div class="form-field">
				<label>	
					<input type="password" name="password" placeholder="Nueva contraseña" required="required" class="{{ $errors->has('password') ? 'error' : '' }}" />
				</label>
			</div>

			<div class="form-field">
				<label>
					<input type="password" name="password_confirmation" placeholder="Repetir contraseña" required="required" class="{{ $errors->has('password') ? 'error' : '' }}"/>
				</label>
			</div>

			<div class="foot">
				<input type="submit" class="button" value="Cambiar" />
			</div>
		</form>
	</div>
</div>
@endsection
