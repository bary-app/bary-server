@extends('layouts.restaurant')

@section('main_content')
@parent
@php ($role = Auth::guard()->user()->hasRoleAt($restaurant->id))
{{ Form::open(['route' => ['restaurant.menu.save', $restaurant->id], 'method' => 'POST']) }}
@if($role == 'A')
<h4><!--Menú: -->
	<div class="right">
		<a class="button small grey" type="button" id="create_section_js" alt="Arrastre o haga click para crear" title="Arrastre o haga click para crear"><i class="la la-plus" alt=""></i> Sección</a>
		<a class="button small grey" type="button" id="create_item_js" alt="Arrastre o haga click para crear" title="Arrastre o haga click para crear"><i class="la la-plus" alt=""></i> Item</a>
		<button class="button small" type="submit" id="save">Guardar</button>
	</div>
</h4>
@endif
<div class="menu @if($role == 'A') editable @endif">@foreach($restaurant->menu as $menu_section)
	<div class="menu-section" data-id="{{$menu_section->id}}">
		<input type="hidden" name="menu_section[]" value='{"id": "{{$menu_section->id}}"}'/>
		@if($role == 'A')
		<div class="toolbar">
			<a href="#" class="edit_si_js" data-tooltip-content='#edit_section' alt="Editar sección"><i class="la la-pencil" alt=""></i></a>
			<a href="#" class="delete_si_js" alt="Borrar sección"><i class="la la-trash" alt=""></i></a>
		</div>
		@endif
		<div class="menu-section-head">
			<h3>
				@if($menu_section->icon)
				<i class="eat eat-{{$menu_section->icon}}"></i>
				@endif
				<font>{{$menu_section->title}}</font>
			</h3>
		</div>
		<div class="menu-items">@foreach($menu_section->items as $menu_item)
			<div class="menu-item" data-id="{{$menu_item->id}}">
				<input type="hidden" name="menu_item[]" value='{"id": "{{$menu_item->id}}"}'/>
				@if($role == 'A')
				<div class="toolbar">
					<a href="#" class="edit_si_js" data-tooltip-content='#edit_item' alt="Editar item"><i class="la la-pencil" alt=""></i></a>
					<a href="#" class="delete_si_js" alt="Eliminar item"><i class="la la-trash" alt=""></i></a>
				</div>
				@endif
				<div class="menu-item-head">
					<h5>{{$menu_item->name}}</h5>
					<p>{{$menu_item->description}}</p>
				</div>
				<div class="menu-item-right">
					<font class="price">{{$menu_item->price}}€</font>
				</div>
			</div>
		@endforeach</div>
	</div>
@endforeach</div>
{{ Form::close() }}
<div class="tooltip_templates">
	<div id="edit_section">
		<div class="option">
			<label>Icono:
				<div class="icons">
					@foreach(\App\RestaurantMenuSection::$ICONS as $icon)
					<a href="#"><i class="eat eat-{{$icon}}"></i></a>
					@endforeach
				</div>
			</label>
		</div>
		<div class="option">
			<label>
				Titulo:
				<input type="text" name="title" autofocus="" />
			</label>
		</div>
	</div>
	<div id="edit_item">
		<div class="option">
			<label>
				Nombre:
				<input type="text" name="name" autofocus/>
			</label>
		</div>
		<div class="option">
			<label>
				Descripción:
				<input type="text" name="description"/>
			</label>
		</div>
		<div class="option">
			<label>
				Precio:
				<input type="number" name="price" step="0.01" min="0.05"/>
			</label>
		</div>
	</div>
</div>
@push('styles')
<link rel="stylesheet" type="text/css" href="/assets/css/eat-icons.css"/>
<link rel="stylesheet" type="text/css" href="/manager/css/menu-creator.css"/>
@if($role == 'A')
<link rel="stylesheet" type="text/css" href="/manager/css/tooltipster.bundle.min.css"/>
@endif
@endpush
@push('scripts')
@if($role == 'A')
<script type="text/javascript" src="/manager/js/tooltipster.bundle.min.js"></script>
<script type="text/javascript" src="/manager/js/menu-creator.js"></script>
@endif
@endpush
@endsection