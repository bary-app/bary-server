@extends('layouts.restaurant')

@section('main_content')
@parent
<div class="subnav">
	<a href="{{route('restaurant.settings.info',$restaurant->id)}}" @if(Route::current()->getName() == 'restaurant.settings.info') class="active" @endif>Información</a>
	<a href="{{route('restaurant.settings.managers',$restaurant->id)}}" @if(Route::current()->getName() == 'restaurant.settings.managers') class="active" @endif>Managers</a>
</div>
@yield('settings_content')
@endsection