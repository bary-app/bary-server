@extends('layouts.app')

@section('content')
<div id="presentation">
    <div class="title">
        <div class="center">
            <div class="card">
                <div class="card-content">
                    <h2>Login</h2>

                    @if ($errors->has('email') || $errors->has('password'))
                    <div class="msg error">
                        <strong>Credenciales incorrectas</strong>
                    </div>
                    @endif

                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-field">
                            <label>
                                <input id="email" type="email" placeholder="Email" name="email" class="{{ $errors->has('email') || $errors->has('password') ? ' error' : '' }}" value="{{ old('email') }}" required autofocus>
                            </label>
                        </div>

                        <div class="form-field">
                            <label>
                                <input id="password" type="password" placeholder="Contraseña" class="{{ $errors->has('email') || $errors->has('password') ? ' error' : '' }}" name="password" required>
                            </label>
                        </div>

                        <div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordarme
                                </label>
                            </div>
                        </div>

                        <div class="foot">
                            <a href="{{ route('password.request') }}">
                                Recuperar contraseña
                            </a>

                            <button type="submit" class="button">
                                Login
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="background"></div>
</div>

@push('styles')
<link rel="stylesheet" type="text/css" href="/manager/css/presentation.css"/>
<link rel="stylesheet" type="text/css" href="/manager/css/auth-card.css"/>
<style type="text/css">
header .right{
    display: none !important;
}
</style>
@endpush
@endsection
