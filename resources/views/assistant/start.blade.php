@extends('layouts.assistant')

@section('content')
@component('components.portrait')
Crear cuenta
@endcomponent
<section class="background-white padding-percent">
    {{ Form::open(['class' => 'form max-medium', 'route' => 'assistant.start', 'method' => 'POST']) }}

    <h3>Datos del usuario</h3>
    <p class="info">Para empezar a administrar tu restaurante debes registrarte en nuestra plataforma para managers:</p>

    <div class="columns col2 colm1">
        <div class="form-field">
            <label>Nombre
                <input type="text" class="{{ $errors->has('name') ? 'error' : '' }}" name="name" value="{{ old('name') }}" required autofocus/>
            </label>

            @if ($errors->has('name'))
            <span class="msg error">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-field">
            <label>Correo-electrónico
                <input type="email" class="{{ $errors->has('email') ? 'error' : '' }}" name="email" value="{{ old('email') }}" required/>
            </label>

            @if ($errors->has('email'))
            <span class="msg error">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-field">
            <label>Contraseña
                <input type="password" class="{{ $errors->has('password') ? 'error' : '' }}" name="password" required/>
            </label>

            @if ($errors->has('password'))
            <span class="msg error">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-field">
            <label>Confirmar contraseña
                <input type="password" name="password_confirmation" required/>
            </label>
        </div>
    </div>

    <div class="foot">
        <button type="submit" class="button success">
            Siguiente
        </button>
    </div>
    {{ Form::close() }}
</section>

@endsection
