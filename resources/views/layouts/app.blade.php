<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/x-icon" href="/favicon.ico">
    <title>Bary manager: Administra tu restaurante comodamente!</title>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="/assets/css/app.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/line-awesome.min.css"/>
    
    <link rel="stylesheet" type="text/css" href="/manager/css/jquery-ui.min.css"/>
    <link rel="stylesheet" type="text/css" href="/manager/css/app.css"/>
    <link rel="stylesheet" type="text/css" href="/manager/css/pres.css"/>
    @stack('styles')
</head>
<body>
    <header>
        <div class="center">
            <a href="/manager" class="logo">
                <img src="/assets/img/sm_icon.png">
                <span class="name">Bary for managers</span>
            </a>

            <div class="right">
                @guest
                <a href="/manager/login">Acceder</a>
                <a href="{{route('assistant.start')}}" class="button small"> <i class="la la-cutlery"></i> Empezar</a>
                @else
                <a href="/manager/home" class="button small"><i class="la la-external-link-square"></i> Administrar</a>
                @endguest
            </div>
        </div>
    </header>
    @yield('content')

    <!-- Scripts -->
    <script type="text/javascript" src="/manager/js/jquery.min.js"></script>
    @stack('scripts')
</body>
</html>
