<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/x-icon" href="/favicon.ico">
    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="/assets/css/line-awesome.min.css" type="text/css" rel="stylesheet"/>
    <link href="/assets/css/app.css" type="text/css" rel="stylesheet"/>
</head>
<body>
    <header>
        <div class="center">
            <a href="/" class="logo">
                <img src="/assets/img/sm_icon.png">
                <span class="name">Bary</span>
            </a>
        </div>
    </header>
    <main>
        @yield('content')
    </main>
    <!-- Scripts -->
    @stack('scripts')
</body>
</html>