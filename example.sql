INSERT INTO `admin` (`id`, `email`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1, 'admin@bary.com', '$2y$10$Ru56Y.Vxt4svkRQdcnVa2ObkpYhS8ignJmsL3T7P2k3MeNYnX1EUG', '', NULL, NULL);

INSERT INTO `city` (`id`, `name`, `country`)
VALUES
	(3, 'Barcelona', 'ES'),
	(4, 'Ibiza', 'ES'),
	(2, 'Madrid', 'ES'),
	(1, 'Valencia', 'ES');

INSERT INTO `manager` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1, 'Arthur', 'arthur@mail.com', '$2y$10$25GVqZTjfT5GKlII53VYrenpJfemHJLIWvAS/xt13Jujp0xXdyZlu', 'fZI6FKdiBo2JySGHv1FUZ8WTUsZSrmKZiA3XM8MvICYCWGmcjnXr7dJU2Vpv', '2018-01-26 15:03:11', '2018-01-26 15:03:11'),
	(2, 'Jaume', 'jaumesv.jmr@gmail.com', '$2y$10$n6gIfTbOrU7wm4DbeFvPZeMhkE8DuJZW6McvA5Sprq5kE9fDCLybC', 'IjDchqE6IrwYcIntvDPVqEfa4GU1lFkz90p2TS4801lvAUPCpiFsHYhcSrtb', '2018-01-29 20:11:52', '2018-03-29 19:51:19'),
	(4, 'Xume', 'xumet12@icloud.com', '$2y$10$Ru56Y.Vxt4svkRQdcnVa2ObkpYhS8ignJmsL3T7P2k3MeNYnX1EUG', 'ndgzAR2C4O3uOqDUZLADE9GzT50W9GNbbAkKb8t9PvdwxJXJOJfpe1c61XVw', '2018-05-08 20:07:39', '2018-05-08 20:07:39');

INSERT INTO `restaurant_type` (`id`, `name`)
VALUES
	(1, 'Comida Rápida'),
	(2, 'Creativo'),
	(3, 'Asiatico'),
	(4, 'Latino'),
	(5, 'Vegetariano'),
	(6, 'Marisqueria'),
	(7, 'Asador'),
	(8, 'Americano'),
	(9, 'Griego'),
	(10, 'Chino'),
	(11, 'Mediterráneo'),
	(12, 'Arrocería'),
	(13, 'Español'),
	(14, 'Pizzería'),
	(15, 'Italiano');

INSERT INTO `restaurant` (`id`, `name`, `address`, `phone`, `description`, `restaurant_type_id`, `city_id`, `approbed`, `created_at`, `updated_at`)
VALUES
	(2, 'Aaaa', 'C/ Pintor Sorolla', NULL, '...', 1, 1, 0, NULL, NULL),
	(17, 'Di francesco', 'historiador Jones n 4, 46232', NULL, '...', 14, 1, 1, '2018-02-04 13:39:22', '2018-02-04 13:39:22'),
	(18, 'La cultura', 'C/ Germans Sotelo n22, Castellar-Oliveral 46026', '+3496396260', 'Lorem ipsum....', 11, 1, 1, '2018-03-05 16:18:21', '2018-03-05 16:18:21'),
	(19, 'Pizza Gulu', 'Plaça de la Policia Local 46015', '963943823', 'La cocina italiana con sabor mediterráneo se descubre en el barrio del Campanar, en Valencia. Pizza Gurú es un excelente representante de un tipo de gastronomia reconocida en todo el mundo.', 14, 1, 1, '2018-03-20 15:04:14', '2018-03-20 15:04:14'),
	(20, 'El huevo', 'Carrer de Salamanca 31 46145', '963964563', 'Platos originales y bien elaborados', 11, 1, 1, '2018-03-20 15:12:12', '2018-03-20 15:12:12'),
	(21, 'Flanky\'s', 'Carrer de Manuel 42143', '963954657', '...', 8, 1, 1, '2018-03-20 15:29:50', '2018-03-20 15:29:50'),
	(22, 'Paprika Bonita', 'Av. del Baró, 32 45721', '963954632', 'El restaurante Paprika Bonita se encuentra en el corazón de Valencia. Su filosofía culinaria se decanta por los productos ecológicos y una dieta saludable con esencia principalmente mediterránea.', 11, 1, 1, '2018-03-20 17:14:35', '2018-03-20 17:14:35'),
	(23, 'Hola', 'C/ Desconocida jajajajaj', '684345223', '...', 7, 1, 0, '2018-05-06 15:50:35', '2018-05-06 19:00:47');

INSERT INTO `restaurant_closed_day` (`restaurant_id`, `date`)
VALUES
	(17, '2018-02-17'),
	(17, '2018-02-25'),
	(17, '2018-02-28'),
	(18, '2018-03-06');

INSERT INTO `restaurant_manager` (`manager_id`, `restaurant_id`, `role`, `activation`)
VALUES
	(1, 18, 'A', NULL),
	(1, 19, 'A', NULL),
	(2, 17, 'A', NULL),
	(2, 22, 'A', NULL),
	(2, 23, 'A', NULL),
	(3, 20, 'A', NULL),
	(3, 21, 'A', NULL),
	(4, 17, 'W', NULL);

INSERT INTO `restaurant_menu_section` (`id`, `icon`, `title`, `restaurant_id`, `order`)
VALUES
	(1, 'pizza', 'Pizzas', 17, 4),
	(2, 'hamburger', 'Hamburguesas', 17, 2),
	(16, '', 'Postre', 20, 3),
	(17, '', 'Entrantes', 21, 1),
	(18, '', 'Plato', 21, 2),
	(21, 'tea', 'Desayuno', 22, 1),
	(22, 'tapas', 'Entrante', 22, 2);


INSERT INTO `restaurant_menu_item` (`id`, `name`, `description`, `price`, `restaurant_menu_section_id`, `order`)
VALUES
	(1, 'Diabola', 'Tomate, queso, salami', 7.50, 1, 3),
	(2, 'María', 'ternera y queso cheddar', 6.50, 2, 1),
	(14, 'Gloría', 'ternera, tomate, lechuga, cebolla, queso y panceta', 7.80, 2, 2),
	(15, 'Gourmet', 'ternera, foie, parmesano, cebolla balsámica y rúcula', 9.00, 2, 3),
	(33, 'Brownie', 'Chocolate y remolacha con helado de jengibre', 4.90, 16, 1),
	(34, 'Tarta de queso', 'con frutos secos', 4.90, 16, 2),
	(35, 'Tiras de pollo crujientes ', '200gr de crujientes tiras de pollo con salsa BBQ para dipear. Crunchy chicken fingers with BBQ sauce.', 4.95, 17, 1),
	(36, 'Quesadilla con bacon, cherry, queso ahumado, ', 'Tortillas de trigo y maíz rellenas con bacon, champiñones, lechuga, tomate cherry, queso ahumado y la deliciosa salsa Franky’s. ', 6.50, 17, 2),
	(37, 'Orden de 3 tacos, carnitas y cochinita pibil ', 'Carne asada de cerdo, lechuga, guacamole, frijoles refritos, salsa BBQ y queso ahumado', 7.50, 17, 3),
	(40, 'Angus Burger ', 'Hamburguesa de 200 gr elaborada 100% con Ternera Angus española. Acompañada de lechuga, tomate y cebolla caramelizada, queso parmesano y salsa casera de trufa, sobre pan de obrador artesano. ', 12.95, 18, 1),
	(41, 'Thanksgiving Burger ', 'Hamburguesa de 200gr de Ternera Gallega acompañada de cerdo desmechado y bacon, con cebolla asada y 2 quesos Monterrey Jack y Cheddar y salsa BBQ. ', 10.50, 18, 2),
	(42, 'Pork Ribs Louisiana', 'Costillar de cerdo asado a baja temperatura con la deliciosa salsa Barbacoa un poco especiada acompañada de patatas fritas o asada', 11.95, 18, 3),
	(47, 'Basic', 'Café o Té. Tostadas con (tomate/mantequilla.mermelada) y zumo de Naranja', 3.40, 21, 1),
	(48, 'Continental', 'Café o Té. Cruasán o tostadas (tomate/mantequilla y mermelada/hummus) zumo o licuado', 5.50, 21, 2),
	(49, 'New york', 'Café o Té. Tostadas con (jamón york sin fostatos / serrano / queso) tomate. zumo o licuado', 5.75, 21, 3),
	(50, 'Hummus Paprika', 'Hummus de judias blancas con paté de olivas y tomates secos', 7.10, 22, 1);

INSERT INTO `restaurant_photo` (`path`, `format`, `description`, `restaurant_id`, `main`, `created_at`, `updated_at`)
VALUES
	('7f00ae899318b15948828ae266e015c3d6ec3', 'jpg', 'Pasta', 17, 0, '2018-05-04 19:32:34', '2018-05-06 16:22:05'),
	('849fb967e94608c444604b565696ea9f72977', 'jpg', 'Entrantes', 17, 0, '2018-05-03 20:03:17', '2018-05-04 20:36:52'),
	('9ee4fe5d578c73cdde8a392fb820ca1ac0a80', 'jpg', 'Pizza caprichosa', 19, 1, '2018-03-20 15:05:20', '2018-03-20 15:05:20'),
	('a6d6b7edc47b35d1f2117663137e396f51142', 'jpg', 'Tristan burger', 17, 1, '2018-05-04 19:32:34', '2018-05-06 16:22:05'),
	('d29859a6a02b0006e7ee1e3ff717444bb10c0', 'jpg', 'Vista de la sala', 21, 1, '2018-03-20 15:30:39', '2018-03-20 15:30:39'),
	('d71117f70d023f100883c1c6f03c0a2ff48ef', 'jpg', 'Sugerencia 2', 20, 1, '2018-03-20 15:13:44', '2018-03-20 15:13:44'),
	('dd24680f76a99153fcd2858e18fe42bf88af5', 'jpg', 'Sugerencia 3', 20, 0, '2018-03-20 15:13:44', '2018-03-20 15:13:44'),
	('e4682c7eaaa7a7b248fd0d9d10f37c0249df6', 'jpg', 'Hamburguesa', 21, 0, '2018-03-20 15:31:14', '2018-03-20 15:31:14');

INSERT INTO `restaurant_tag` (`id`, `name`)
VALUES
	(1, 'Moderno'),
	(2, 'Arroces'),
	(3, 'Divertido'),
	(4, 'Pizzas'),
	(5, 'Mediterraneo'),
	(6, 'Padel'),
	(7, 'Almuerzo'),
	(8, 'Italiano'),
	(9, 'Saler'),
	(10, 'Pasta'),
	(11, 'Ensaladas'),
	(12, 'Domicilio'),
	(13, 'Con amigos'),
	(14, 'Campanar'),
	(15, 'Carnes'),
	(16, 'Pizza'),
	(17, 'Vegetarianos'),
	(18, 'Eixample'),
	(19, 'Veganos'),
	(20, 'Aqua');

INSERT INTO `restaurant_restaurant_tag` (`restaurant_id`, `restaurant_tag_id`)
VALUES
	(2, 1),
	(17, 3),
	(17, 4),
	(18, 5),
	(18, 6),
	(18, 7),
	(17, 8),
	(19, 8),
	(17, 9),
	(17, 10),
	(19, 10),
	(17, 11),
	(17, 12),
	(17, 13),
	(19, 14),
	(19, 15),
	(21, 15),
	(19, 16),
	(19, 17),
	(20, 17),
	(21, 17),
	(20, 18),
	(20, 19),
	(21, 20);

INSERT INTO `user` (`id`, `name`, `surname`, `dni`, `genre`, `email`, `phone`, `password`, `is_verified`, `remember_token`, `verification_code`, `created_at`, `updated_at`)
VALUES
	(1, 'Pedro', 'Sim', '21007509E', 'M', 'jsegarra@iesfuentesanluis.org', NULL, '$2y$10$YJsSicGpvWR8/ReJLzYZbutta.tHAmznVr3SvlPq4Pde34.qae9VC', 1, NULL, NULL, '2018-01-25 19:37:09', '2018-03-24 17:08:53'),
	(2, 'Jaume', 'Segarra', '71230169C', 'M', 'jaumesv.jmr@gmail.com', 963943823, '$2y$10$YSlpjBXMfS.NxT4ul5cjIeGoOTa8gfQxehAnfzYPjDaFkb7A6o1tm', 1, NULL, NULL, '2018-01-27 10:45:15', '2018-04-23 20:04:04'),
	(3, 'Victor', 'Ruiz', NULL, 'M', 'ja.um.esv.jmr@gmail.com', NULL, '$2y$10$dNfaAmqdJS8SAdsRgol9QuhhoWUi7D6Ybxe8EyTmS5IdGQ8SlD3Cq', 0, NULL, 'SkN14xpwrR06mgarTjE4QgXjyjSw6H', '2018-03-20 18:46:47', '2018-03-20 18:46:47'),
	(9, 'Bary', 'dfdf', NULL, 'F', 'j.aum.esv.jmr@gmail.com', NULL, '$2y$10$oSoP/c3QaNTQHcs.1r9nauj4vMxLtoGMBwZApvaALE9Wk.j59y4C.', 0, NULL, 'qzrwKrS0okIt7ZytnNKwTAVmMCuo4k', '2018-03-20 19:10:55', '2018-03-20 19:10:55');

INSERT INTO `user_restaurant_favourite` (`restaurant_id`, `user_id`)
VALUES
	(17, 1),
	(17, 2),
	(20, 2),
	(21, 2);
