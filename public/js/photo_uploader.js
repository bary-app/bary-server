$('#fine-uploader-manual-trigger').fineUploader({
	template: 'qq-template-manual-trigger',
	request: {
		endpoint: $("input[name='_endpoint']").val()
	},
	validation: {
		allowedExtensions: ['jpeg', 'jpg', 'png'],
		sizeLimit: 3072000
	},
	/*callbacks: {
		onComplete:function(id, name, resp){
			console.log(id, name, resp);
		}
	},*/
	autoUpload: false
})
.on('upload', function(event, id, name) {
	var $fileItemContainer = $(this).fineUploader('getItemByFileId', id);
	var $description = $('input[name="description"]', $fileItemContainer);

	$(this).fineUploader('setParams', {"_token": $('input[name="_csrf"]').val(), "description": $description.val()}, id);
}).on('complete', function(event, id, name){
	console.log(event);

	var $fileItemContainer = $(this).fineUploader('getItemByFileId', id);
	var $description = $('input[name="description"]', $fileItemContainer);

	$description.attr("disabled", "disabled");
});

$('#trigger-upload').click(function() {
	var $descriptionFields = $("input[name='description']");
	var $emptyDescriptionFields = $descriptionFields.filter(function() { return $(this).val().trim() == ""; });

	if($emptyDescriptionFields.length == 0){
		$descriptionFields.removeClass('error');
		$('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
	}
	else
		$emptyDescriptionFields.addClass('error');
});