	function tooltip($element) {
		$('.edit_si_js', $element).tooltipster({ // Habilitar toolstip para editar
			trigger:'custom', 
				triggerOpen: { // Abrir tooltip al
					click:true // hacer click
				},
				triggerClose:{ // Cerrar tooltip al
					originClick:true, // hacer click en el enlace
					scroll:true, // al hacer scroll del body
					mouseleave:true // al quitar el ratón del tooltip
				},
				interactive:'true', // Permite poder hacer click dentro del toolstip sin cerrarlo
				animation: 'grow',
				side:'right',
				contentCloning: true, // Crea un toolstip nuevo a partir de la plantilla
				functionReady: function(e){ // Al abrir el toolstip					
					var $this = e._$tooltip; // Obtener el toolstip
					var $siel = e._$origin.parent().parent(); // Obtener el elemento del formulario
					
					$siel.addClass("editing");

					/*
					Llamar a la función que permite editar este tipo de elemento.
					Ejemplo: initEdit_text(...)
					*/
					if($siel.hasClass("menu-section"))
						edit_section_tooltip($this, $siel);
					else
						edit_item_tooltip($this, $siel);
				},
				functionAfter: function(e){ // Al cerrar el toolstip

					var $siel = e._$origin.parent().parent(); // Obtener el elemento del formulario
					$siel.removeClass("editing");
				}
			});
	}

	function edit_section_tooltip($tooltip, $el) {
		var $titleIpt = $('input[name="title"]', $tooltip);

		var $icon = $(".menu-section-head > h3 > i", $el);
		var $title = $(".menu-section-head > h3 > font", $el);

		var $inputEl = $('input[name="menu_section[]"]', $el);
		var el = JSON.parse($inputEl.val());

		function get_icon_name($icon) {
			return $icon.attr("class").replace("eat", "").replace("eat-", "").trim();
		}

		if($icon.length > 0){
			var icon_name = get_icon_name($icon);

			$('.icons a > i.eat[class~="eat-'+icon_name+'"]', $tooltip).parent().addClass('selected');
		}
		$('.icons a', $tooltip).on('click', function (e) {
			e.preventDefault();
			
			if($(this).hasClass('selected')){
				$('.icons a', $tooltip).removeClass('selected');
				el.icon = "";
				$icon.remove();
				$icon = undefined;
			}else{
				$('.icons a', $tooltip).removeClass('selected');
				el.icon = get_icon_name($('i', this));

				$(this).addClass('selected');
				if(!$icon || $icon.length == 0){
					$icon = $("<i class=''></i>");
					$(".menu-section-head > h3", $el).prepend($icon);
				}
				$icon.attr("class", "eat eat-"+el.icon);
			}
			$inputEl.val(JSON.stringify(el));
		});

		var tmp_title = $title.text().trim();
		var title_timeout;
		$titleIpt.val(tmp_title);
		$titleIpt.on("keyup", function () {
			var text = $(this).val();
			$title.text(text);

			if(text.trim() == ""){
				title_timeout = setTimeout(function () {
					$title.text(tmp_title);
					$titleIpt.val(tmp_title);
				},250);
			}else{
				if(title_timeout) clearTimeout(title_timeout);
				tmp_title = text;
				el.title = text;
				$inputEl.val(JSON.stringify(el));
			};
		});
	}
	
	function edit_item_tooltip($tooltip, $el) {
		var $nameIpt = $('input[name="name"]', $tooltip);
		var $descriptionIpt = $('input[name="description"]', $tooltip);
		var $priceIpt = $('input[name="price"]', $tooltip);

		var $name = $(".menu-item-head > h5", $el);
		var $description = $(".menu-item-head > p", $el);
		var $price = $(".menu-item-right > font.price", $el);

		var $inputEl = $('input[name="menu_item[]"]', $el);
		var el = JSON.parse($inputEl.val());

		var tmp_name = $name.text().trim();
		var name_timeout;
		$nameIpt.val(tmp_name);
		$nameIpt.on("keyup", function () {
			var text = $(this).val();
			$name.text(text);

			if(text.trim() == ""){
				name_timeout = setTimeout(function () {
					$name.text(tmp_name);
					$nameIpt.val(tmp_name);
				},250);
			}else{
				if(name_timeout) clearTimeout(name_timeout);
				tmp_name = text;
				el.name = text;
				$inputEl.val(JSON.stringify(el));
			};
		});

		var tmp_description = $description.text().trim();
		var description_timeout;
		$descriptionIpt.val(tmp_description);
		$descriptionIpt.on("keyup", function () {
			var text = $(this).val();
			
			if(text.trim() == ""){
				description_timeout = setTimeout(function () {
					$description.text(tmp_description);
					$descriptionIpt.val(tmp_description);
				},250);
			}else{
				if(description_timeout) clearTimeout(description_timeout);
				tmp_description = text;
				el.description = text;
				$inputEl.val(JSON.stringify(el));

				$description.text(text);
			};
		});

		var tmp_price = $price.text().replace("€", "");
		var price_timeout;
		$priceIpt.val(tmp_price);
		function priceIpt_change() {
			var text = $(this).val();
			if(text.trim() == "" || !$.isNumeric(text) || +text < 0.01){
				console.log('ss');
				price_timeout = setTimeout(function () {
					$price.text(tmp_price);
					$priceIpt.val(tmp_price);
				},250);
			}else{
				if(price_timeout) clearTimeout(price_timeout);
				tmp_price = text;
				el.price = +text;
				$inputEl.val(JSON.stringify(el));

				$price.text(text+"€");
			}
		}

		$priceIpt.on("change", priceIpt_change);
		$priceIpt.on("keyup", priceIpt_change);
	}

	$(function() {
		$(".menu").sortable({
			beforeStop: function (event, ui) { // Antes de insertar un nuevo elemento
			$element = ui.item; // guardar elemento en la variable
		},
		cancel: ".deleted",
		receive: function(event, ui) { // Al insertarlo
			/*
			* Solucionar problema con jquery ui que ponia un width y height estáticos
			*/
			$element.css("width", "");
			$element.css("height", "");

			init_section_element($element);
		},
		stop: function (event, ui) {
			$(".menu-section", event.target).each(function(index) {
				var $input = $('input[name="menu_section[]"]', this);
				var el = JSON.parse($input.val());
				el.order = index+1;
				$input.val(JSON.stringify(el));
			});
		}
	});

		function sort_items($el) {
			if($('.menu-item', $el).length > 0)
				$('.menu-item', $el).each(function(index) {
					var $input = $('input[name="menu_item[]"]', this);
					var el = JSON.parse($input.val());
					el.order = index+1;
					$input.val(JSON.stringify(el));
				});
			else
				$($el).html('');
		}

		function menu_items_sortable($el) {
			$el.sortable({
				connectWith: ".menu-items",
				beforeStop: function (event, ui) { // Antes de insertar un nuevo elemento
			$element = ui.item; // guardar elemento en la variable
		},
		receive: function(event, ui) { // Al insertarlo
			/*
			* Solucionar problema con jquery ui que ponia un width y height estáticos
			*/
			$element.css("width", "");
			$element.css("height", "");

			init_item_element($element);
		},
		cancel: ".deleted",
		stop: function (event, ui) {
			if(!$(event.target).is(ui.item.parents(".menu-items"))){
				var $input = $('input[name="menu_item[]"]', ui.item);
				var el = JSON.parse($input.val());
				el.section_id = $(ui.item).parents(".menu-section").data("id");
				$input.val(JSON.stringify(el));

				sort_items(ui.item.parents(".menu-section"));
			}

			sort_items(event.target);
		}
	});
		}

		menu_items_sortable($(".menu-items"));
		tooltip('.menu');

		var temp_section_id = 0;
		function create_section_element() {
			temp_section_id--;

			var info = {
				"id": temp_section_id,
				"title": 'Nueva sección'
			};

			var $div = $('<div></div>');
			$div.addClass('menu-section');
			$div.attr('data-id', info.id);

			var $input_data = $('<input type="hidden" name="menu_section[]"/>');
			$input_data.val(JSON.stringify(info));
			$div.append($input_data);

			var $section_head = $('<div class="menu-section-head"><h3><font></font></h3></div>');
			$('font', $section_head).text(info.title);
			$div.append($section_head);

			var $menu_items = $('<div class="menu-items"></div>');
			$div.append($menu_items);

			$toolbar = $('<div class="toolbar"><a href="#" class="edit_si_js" data-tooltip-content="#edit_section" alt="Editar sección"><i class="la la-pencil" alt=""></i></a><a href="#" class="delete_si_js" alt="Borrar sección"><i class="la la-trash" alt=""></i></a></div>');
			$div.prepend($toolbar);

			return $div;
		}

		function init_section_element($el) {
			menu_items_sortable($('.menu-items', $el));
			tooltip($el);
		}

		$("#create_section_js").draggable({ // Permitir arrastrar
		revert: "invalid", // Dehacer al arrastrar fuera
		helper: function() { // Substituir el botón de arrastre por el nuevo elemento a crear
			return create_section_element(); // Crear elemento arrastrable
		},
			connectToSortable: ".menu", // Permitir ser arrastrado al formulario
			cursor: "move"
		});

		$("#create_section_js").on("click", function () {
			var $el = create_section_element();
			$menu = $(".menu");

			var order = $(".menu > .menu-section").length+1;
			
			var el = JSON.parse($('input[name="menu_section[]"]', $el).val());
			el.order = order;
			$('input[name="menu_section[]"]', $el).val(JSON.stringify(el));

			$menu.append($el);

			init_section_element($el);
		});

		var temp_item_id = 0;
		function create_item_element() {
			temp_item_id--;

			var info = {
				"id": temp_item_id,
				"name": 'Nuevo item',
				"description": 'ingredientes',
				"price": 9.99
			};

			var $div = $('<div></div>');
			$div.addClass('menu-item');
			$div.attr('data-id', info.id);

			var $input_data = $('<input type="hidden" name="menu_item[]"/>');
			$input_data.val(JSON.stringify(info));
			$div.append($input_data);

			var $item_head = $('<div class="menu-item-head"></div>');

			var $h5 = $("<h5>"+info.name+"</h5>");
			$item_head.append($h5);

			var $p = $("<p>"+info.description+"</p>");
			$item_head.append($p);

			$div.append($item_head);

			var $menu_item_price = $('<div class="menu-item-right"><font class="price">'+info.price+'€</font></div>');
			$div.append($menu_item_price);

			$toolbar = $('<div class="toolbar"><a href="#" class="edit_si_js" data-tooltip-content="#edit_item" alt="Editar item"><i class="la la-pencil" alt=""></i></a><a href="#" class="delete_si_js" alt="Borrar item"><i class="la la-trash" alt=""></i></a></div>');
			$div.prepend($toolbar);

			return $div;
		}

		function init_item_element($el) {
			var $input = $('input[name="menu_item[]"]', $el);
			var el = JSON.parse($input.val());
			el.section_id = $el.parents(".menu-section").data("id");
			$input.val(JSON.stringify(el));

			tooltip($el);
		}

		$("#create_item_js").draggable({ // Permitir arrastrar
		revert: "invalid", // Dehacer al arrastrar fuera
		helper: function() { // Substituir el botón de arrastre por el nuevo elemento a crear
			return create_item_element(); // Crear elemento arrastrable
		},
			connectToSortable: ".menu-items", // Permitir ser arrastrado al formulario
			cursor: "move"
		});

		$("#create_item_js").on("click", function () {
			var $menu_section = $(".menu > .menu-section:last");

			if ($menu_section.length > 0){
				var $el = create_item_element();
				$menu_items = $(".menu-items", $menu_section);
				$menu_items.append($el);
				sort_items($menu_items);

				init_item_element($el);
			}
		});

		$('.menu').on("click", ".edit_si_js", function (e) { e.preventDefault();});

		$('.menu').on("click", ".delete_si_js", function (e) {
			e.preventDefault();
			var $el = ($(this).parents(".menu-item").length > 0) ? $(this).parents(".menu-item") : $(this).parents(".menu-section");
			$el.addClass("deleted");

			$input = ($('input[name="menu_section[]"]', $el).length > 0) ? $('input[name="menu_section[]"]', $el) : $('input[name="menu_item[]"]', $el);
			var el = JSON.parse($input.val());
			el.deleted = 1;
			$input.val(JSON.stringify(el));

			var $name = ($(".menu-section-head > h3", $el).length > 0) ? $(".menu-section-head > h3", $el) : $(".menu-item-head > h5", $el);
			var name = $name.text().trim();
			var $ban = $("<div class='ban'>Elimado <font class='name'>"+name+"</font>. <a href='#'>Deshacer</a></div>");
			$('a', $ban).click(function (e) {
				e.preventDefault();
				var el = JSON.parse($input.val());
				delete el.deleted;
				$input.val(JSON.stringify(el));

				$el.removeClass("deleted");
				$ban.remove();
			});
			$el.prepend($ban);
		});
	});