var weekColors = ['#ffe0e6', '#ffecd9', '#fff5dd', '#dbf2f2', '#d7ecfb', '#ebe0ff', '#f4f5f5'];
var weekBorderColors = ['#ff7592', '#ffaa56', '#ffd36a', '#60c7c7', '#4eaded', '#a578ff', '#cfd1d4'];

var dataWeekdays = {
	datasets: [{
		data: $('.reserve_days canvas').data('value'),
		backgroundColor: ['#ddd'].concat(weekColors),
		borderColor: ['#ddd'].concat(weekBorderColors)
	}],

	labels: [
	'Sin reservas',
	'Lunes',
	'Martes',
	'Miercoles',
	'Jueves',
	'Viernes',
	'Sabado',
	'Domingo'
	]
};

var doughnutOptions = {
	legend: {
		display: false
	},
	tooltips: {
		callbacks: {
			label: function(tooltipItem, data) {
				if(tooltipItem.index !== 0)
					return data.labels[tooltipItem.index]+': '+data.datasets[0].data[tooltipItem.index]+' pers.';
				else
					return 'Sin reservas';
			}
		}
	},
	responsive: true,
	maintainAspectRatio: false
};
var reserve_days_doughnut = new Chart(document.querySelector('.reserve_days canvas').getContext('2d'), {
	type: 'doughnut',
	data: dataWeekdays,
	options: doughnutOptions
});

function get_hours_labels(){
	var labels = [];
	var date = new moment();
	date.minute(0);
	date.hour(0);

	while(date.format('H:mm') != "23:30"){
		labels.push(date.format('H:mm'));
		date.add(30, 'minutes');
	}
	
	labels.push(date.format('H:mm'));
	date.add(30, 'minutes');
	
	return labels;
};

function get_weekcolor(){
	var day = new moment().weekday()-1;

	return weekBorderColors[((day < 0) ? weekBorderColors.length-1 : day)];
}

var dataHours = {
	datasets: [{
		data: $('.reserve_hours canvas').data('value'),
		backgroundColor: get_weekcolor()
	}],
	labels: get_hours_labels()
};

var lineOptions = {
	legend: {
		display: false
	},
	scales: {
		yAxes: [{
			stacked: true,
			ticks: {
				beginAtZero:true
			}
		}]
	}
};
var reserve_months_line = new Chart(document.querySelector('.reserve_hours canvas'), {
	type: 'bar',
	data: dataHours,
	options: lineOptions
});

$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});

var av_dates = [];
loader_bar(true);
$.ajax({
	type: "GET",
	url: GET_AVAILABLE_DATES
}).done(function(msg) {
	if(msg.length > 0){
		av_dates = msg;
		loadCalendar();
		$('.full_days').show();
	}else
		$('.full_days_none_js').show();
	
}).fail(function(jqXHR, textStatus) {
	console.error(textStatus, jqXHR);
	revert();
}).then(function(){
	loader_bar(false);
});

function loadCalendar(){
	$(".ll-skin-melon .calendar").datepicker({
		onSelect: load_reserves_data,
		beforeShowDay: function(date) {
			var date = moment(date, "DD/MM/YYYY", true).format('YYYY-MM-DD');
			
			return [av_dates && av_dates.indexOf(date) != -1];
		}
	});
	var date = moment(av_dates[0], "YYYY-MM-DD", true).toDate();

	$(".ll-skin-melon .calendar").datepicker("setDate", date);
	load_reserves_data(date);
}

function load_reserves_data(day){
	loader_bar(true);
	var date = moment(day, "DD/MM/YYYY", true).format('YYYY-MM-DD');
	
	$.ajax({
		type: "GET",
		url: GET_RESERVES_DATA+'/'+date
	}).done(function(data) {
		var $data = $('.full_days .reserves_data');
		$data.html('');
		data.hours.forEach( function(element, index) {
			var nr = data.capacity%element.num_pers;
			var $el = $(`
				<div class="reserves_data_day">
				<h4>`+element.hour+` `+(element.num_pers < 1 ? '<span>Completo</span>' : '')+`</h4>
				<font><span>`+nr+`</span>/`+data.capacity+` personas reservadas. (`+nr/data.capacity*100+`%)</font>
				</div>
				`);
			$data.append($el);
		});

	}).fail(function(jqXHR, textStatus) {
		console.error(textStatus, jqXHR);
		revert();
	}).then(function(){
		loader_bar(false);
	});
}